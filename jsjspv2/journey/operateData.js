/**
 * 操作基本数据
 */

/**
 * 通过服务器接口调用微信数据
 *
 * @param {Object}
 *            openId 当前调用用户ID
 * @param {Object}
 *            type 调用类型
 * @param {Object}
 *            data 回调数据
 * @param {Object}
 *            sucesscallback 调用成功回调函数,回调参数：data, status, xhr
 * @param {Object}
 *            errcallback 调用失败回调函数,回调参数：xhr, error, exception
 */
function bidgeToWeixin(openId, type, data, sucesscallback, errcallback) {
		if (openId.indexOf("#") > -1) {
			openId = openId.replace("#", "");
		}
		$.ajax({
			url: actionStr,
			type: 'POST',
			data: JSON.stringify(popRequest(type, openId, popBody(type, data))),
			dataType: 'json',
			contentType: 'application/json'
		}).then(sucesscallback).fail(
			errcallback || function(xhr, error, exception) {
				closemaskLoading();
			});
	}
	/**
	 * 保存数据
	 */

function saveData(data) {
	if (openId.indexOf("#") > -1) {
		openId = openId.replace("#", "");
	}
	var loc = {
		Latitude: 29.579916,
		Longitude: 115.98905
	};

	loc.Latitude = parseFloat(data.Destination.Loc.Latitude) * 1000000;
	loc.Longitude = parseFloat(data.Destination.Loc.Longitude) * 1000000;

	var destination = {
		Name: data.Destination.Name,
		City: data.Destination.City.name || '',
		Loc: loc
	};
	var members = [];
	members.push(openId);

	var dataStr = {
		Title: data.Title,
		ValidDate: data.ValidDate + ' 23:59:59',
		StartDate: data.StartDate + ' 00:00:00',
		Destination: destination,
		MemberStatus: members
	};
	bidgeToWeixin(openId, 'CreateTrip', dataStr, function(data, st, xhr) {
		//alert('创建旅程成功!');
		//window.location.href = "trip_list.html?openid=" + openId;
		removeSaveEditDataToSessionStorage('createTripData');
		window.location.href = "trip_detail.jsp?status=0&isActive=0&tripid=" + data.portalResp.Data.TripID;
	}, function(xhr, error, exception) {
		alert('创建旅程失败!');
	});
}

function editData(data) {
		var loc = {
			Latitude: 29.579916,
			Longitude: 115.98905
		};

		loc.Latitude = parseFloat(data.Destination.Loc.Latitude) *  1000000;
		loc.Longitude = parseFloat(data.Destination.Loc.Longitude) *  1000000;

		var destination = {
			Name: data.Destination.Name,
			City: data.Destination.City.name || '',
			Loc: loc
		};
		var dataStr = {
			TripID: data.TripID,
			Title: data.Title,
			ValidDate: data.ValidDate + ' 23:59:59',
			StartDate: data.StartDate + ' 00:00:00',
			Destination: destination,
			TripLeader: data.TripLeader || ''
		};
		bidgeToWeixin(openId, 'EditTrip', dataStr, function(data, st, xhr) {
			//alert('修改旅程成功!');
			//window.location.href = "trip_list.html?openid=" + openId;
			removeSaveEditDataToSessionStorage('createTripData');
			window.location.href = 'trip_detail.jsp?status=' + status + '&isActive=' + isActive + '&tripid=' + tripId || '';
		}, function(xhr, error, exception) {
			alert('修改旅程失败!');
		});
	}
	/**
	 * 根据条件获取数据,默认返回整个数据集
	 *
	 * @param {Object}
	 *            tripId 需要编辑的旅程ID号
	 * @param {Object}
	 *            callback 回调函数
	 */

function getJourney(tripId, callback) {
	var dataStr = {
		"TripID": tripId
	};
	bidgeToWeixin(openId, 'TripDetail', dataStr, callback);
}

/**
 * 通过Ajax调用接口获取好友列表 ，需要添加js:jsonMsg.js
 *
 * @param {Object}
 *            openId
 * @param {Object}
 *            callback 回调函数
 */

function getFriendsListByAjax(openId, callback) {
		bidgeToWeixin(openId, 'ListFriends', {}, function(data, status, xhr) {
			if (data && data.portalResp.Data.Profiles) {
				callback(data.portalResp.Data.Profiles);
			} else {
				callback(null);
			}
		});
	}
	/**
	 * 界面上一些基本操作
	 */
	
function showAlertMask(c) {
	c.title ? $('.alert-mask .message_text').html(c.title) : '';
	c.button ? $('.alert-mask .messagebtnone').html(c.button) : '';
	$('.alert-mask').show();
}

function closeAlertMask() {
		$('.alert-mask').hide();
	}
function showConfirmMask(c) {
	c.title ? $('.confirm-mask .message_text').html(c.title) : '';
	$('.confirm-mask').show();
}

function closeConfirmMask() {
		$('.confirm-mask').hide();
	}
	/**
	 * iframe页面调用，将选择的值传递过来
	 *
	 * @param {Object}
	 *            render 值绑定对象
	 * @param {Object}
	 *            value 值
	 * @param {Object}
	 *            data 绑定的值
	 */

function setIframeContentValue(win, render, value, data) {
		var ren = $('*[data-column=' + render + ']', $(win.document || document));
		ren.html(value);
		if (render == 'StartDate' || render == 'ValidDate' || render == 'Destination') {
			ren.css({
				'color': '#363636'
			});
		}
		if (data) {
			for (var k in data) {
				ren.data(k, data[k]);
			}
		}
		//closeIframeContent();
	}
	/**
	 * 关闭iframe嵌套窗口
	 */

function closeIframeContent() {
		$('.iframe-containter').hide().empty();
		$('body').css({
			'overflow-y': 'auto'
		});
	}
	/**
	 * 将要调用的二级页面嵌套调用
	 *
	 * @param {Object}
	 *            src 调用路径
	 */

function setIframeSrc(src) {
		$('body').css({
			'overflow-y': 'hidden'
		});
		var iframeContainer = $('.iframe-containter').css({
				'top': $('body').scrollTop() || '0px'
			}).empty().css({
				'left': '0%'
			}).show(),
			iframe = $('<iframe>').attr({
				'src': src
			}).appendTo(iframeContainer);
		iframeWin = iframe[0].contentWindow;
	}
	/**
	 * 获取元素父元素
	 *
	 * @param {Object}
	 *            par 元素,$对象
	 * @param {Object}
	 *            filter 过滤条件
	 */

function _getTarparanet(par, filter) {
	return (par[0][filter['key']] == filter['value'] || par[0][filter['key']] == 'BODY') ? par : _getTarparanet(par.parent(), filter);
}

function _getTarparanetByClass(par, filter) {
		return par.hasClass(filter) ? par : _getTarparanetByClass(par.parent(), filter);
	}
	/**
	 * 初始界面上的数据
	 *
	 * @param {Object}
	 *            data 数据集
	 */

function initJourneyInfo(data) {
		if (data) {
			$('*[data-column]').each(function() {
				var tar = $(this),
					column = tar.attr('data-column'),
					d = data;
				tar.css({
					'color': 'rgb(51, 51, 51)'
				});
				switch (column) {
					case 'Title':
						tar.val(d.Title || '请输入旅途主题(必填)');
						d.Title ? '' : tar.css({
							'color': '#999'
						});
						break;
					case 'StartDate':
						d.StartDate = d.StartDate;
						tar.html(d.StartDate ? d.StartDate.replace(/年|月/g, '-').replace('日', '') : '开始时间');
						d.StartDate ? '' : tar.css({
							'color': '#999'
						});
						break;
					case 'ValidDate':
						tar.html(d.ValidDate ? d.ValidDate.replace(/年|月/g, '-').replace('日', '') : '结束时间');
						d.ValidDate ? '' : tar.css({
							'color': '#999'
						});
						break;
					case 'TripLeader':
						tar.val(d.TripLeader);
						break;

					case 'Destination':
						if (d.Destination) {
							var l = d.Destination.Loc,city=d.Destination.City;
							var ad = {
								Loc: {
									Latitude: l.Latitude / (l.Latitude < 1000 ? 1 : 1000000),
									Longitude: l.Longitude / (l.Latitude < 1000 ? 1 : 1000000),
								},
								Name: d.Destination.Name,
								City: {name:city?city.name:'',citycode:''},
								isCreate: true
							};
							tar.html(d.Destination.Name).data('address', ad);
							setSaveEditDataToSessionStorage('tripAddress', {
								Loc: {
									Latitude: l.Latitude * (l.Latitude > 1000 ? 1 : 1000000),
									Longitude: l.Longitude * (l.Latitude > 1000 ? 1 : 1000000),
								},
								Name: d.Destination.Name,
								City: {name:city?city.name:'',citycode:''}
							});
						} else {
							tar.css({
								'color': '#999'
							});
						}
						break;
						/*
						 * case 'friendslist': var lis = []; for (var i = 0, l = d.length; i <
						 * l; i++) { lis.push($('<li>').data('user', d).append($('<img>').attr({
						 * 'src': d[i].photo }).addClass('photo-img ' + (i == 6 ?
						 * 'user-leader' : ''))) .append($('<span>').addClass('photo-img-marker'))
						 * .append($('<span>').html(d[i].name))); } $('ul',
						 * tar).append(lis); break;
						 */
					case 'destination-view':
						tar.css({
							'color': '#999'
						});
						break;
					default:
						tar.val(d);
						break;
				}
			});
		}
	}
	/**
	 * 更新旅程状态
	 *
	 * @param {Object}
	 *            tripId 旅程ID
	 * @param {Object}
	 *            stats 状态值,Terminate 结束旅程
	 */

function updateTripStatus(tripId, stats) {
		var dataStr = {
			"TripID": tripId,
			"TripStatus": stats
		};
		bidgeToWeixin(openId, 'EditTripStatus', dataStr,
			function(data, status, xhr) {
				if (stats == 'Active') {
					// window.location.href = "trip_voice.html?openid=" + openId
					// + "&tripid=" + tripId;
					window.location.href = "inJourney.jsp?tripid=" + tripId;
				} else {
					window.location.href = "trip_list.jsp";
				}
			});
	}
	/**
	 * 创建加载等待框
	 */

function showMaskLoading() {
		/*$('<div>').addClass('k-mask-container').css({
				'height': window.screen.availHeight + 'px',
				'line-height': window.screen.availHeight + 'px'
			})
			.append($('<div>').addClass('k-mask-logding').html('请稍后...'))
			.appendTo($('body').css({
				'overflow-y': 'hidden'
			}));*/
	}
	/**
	 * 关闭创建加载等待框
	 */

function closemaskLoading() {
		/*$('.k-mask-container').hide('slow').remove();
		$('body').css({
			'overflow-y': 'auto'
		})*/
	}
	/**
	 * 保存编辑数据
	 * @param {string} key 存储关键字
	 * @param {object} data 存储的数据
	 */

function setSaveEditDataToSessionStorage(key, data) {
		var sKey = window.sessionStorage[key] || window.localStorage[key];
		sKey ? removeSaveEditDataToSessionStorage(key) : '';
		if (toString.apply(data) === '[object Object]') {
			if (JSON) {
				data = JSON.stringify(data);
			} else {
				console && console.log && console.log('JSON对象不存在!');
				return false;
			}
		}
		window.sessionStorage[key] = data;
		window.localStorage[key] = data;
	}
	/**
	 * 修改存储值
	 * @param {Object} okey 获取总对象，可以为空
	 * @param {Object} data 需要修改的值
	 */

function mSaveEditDataToSessionStorage(okey, data) {
		var sKey = okey ? getSaveEditDataToSessionStorage(okey) : {};
		sKey = sKey && $ ? $.extend(true, sKey, data) : data;
		setSaveEditDataToSessionStorage(okey, sKey);
	}
	/**
	 * 获取界面保存的数据
	 * @param {Strin} key 需要查询的对象
	 */

function getSaveEditDataToSessionStorage(key) {
		var sKey = window.sessionStorage[key] || window.localStorage[key];
		if (toString.apply(sKey) === '[object String]' && sKey === '[object Object]') {
			return null;
		}
		return sKey ? $.parseJSON(sKey) : null;
	}
	/**
	 * 删除存储的数据
	 * @param {Object} key 关键字
	 */

function removeSaveEditDataToSessionStorage(key) {
		window.sessionStorage.removeItem(key);
		window.localStorage.removeItem(key);
	}
	/**
	 * 弹出窗口
	 * @param {Object} url 弹窗路径
	 * @param {Object} param 弹窗参数
	 */

function openWin(url, param) {
		window.open(url, '_blank', param ? param : 'width=' + window.screen.width + ',height=' + window.screen.height + ',menubar=no,toolbar=no,location=no,directories=no,status=no,scrollbars=yes,resizable=yes');
	}
	/**
	 * 初始化加载时加载
	 */
	/*$ && $(function() {
		$('.js_back').on('click', function() {
			parentwin && parentwin.closeIframeContent();
		});
	});*/

Array.prototype.sum = function() {
	for (var sum = i = 0; i < this.length; i++)
		sum += parseInt(this[i]);
	return sum;
};