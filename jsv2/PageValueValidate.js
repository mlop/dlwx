function inputTxt(txtValue) {

	var forbidChar = new Array("@", "#", "$", "^", "&", "*", "……", "￥", "\"", "’", "'");
	for (var i = 0; i < forbidChar.length; i++) {
		if (txtValue.indexOf(forbidChar[i]) >= 0) {
			showAlertMask?showAlertMask({
				title:"您输入的信息中含有非法字符: " + forbidChar[i] + " 请更正！"
			}):alert("您输入的信息中含有非法字符: " + forbidChar[i] + " 请更正！");
			return true;
		}
	}
	return false;;
}

function getStrLength(strValue) {
	var strLength = strValue.length;
	for (var j = 0; j < strValue.length; j++) {
		if (strValue.charCodeAt(j) > 255) {
			strLength++;
		}
	}
	return strLength;
}

function strDateTime(str) {
	var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;
	var r = str.match(reg);
	if (r == null) return false;
	var d = new Date(r[1], r[3] - 1, r[4], r[5], r[6], r[7]);
	return (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[3] && d.getDate() == r[4] && d.getHours() == r[5] && d.getMinutes() == r[6] && d.getSeconds() == r[7]);
}

var r = /^\+?[1-9][0-9]*$/;　　 //正整数 

function validIntField(fieldName, displayName, fieldLength, allowNull) {
	var name = document.getElementById(fieldName).value;

	if (name == null || name.trim() == "") {
		if (!allowNull) {
			showAlertMask?showAlertMask({title:"请输入" + displayName + "."}):alert("请输入" + displayName + ".");
			document.getElementById(fieldName).select();
			return false;
		}
	} else {
		if (!r.test(name)) {
			showAlertMask?showAlertMask({title:displayName + "必须是整数."}):alert(displayName + "必须是整数.");
			document.getElementById(fieldName).select();
			return false;
		}
		if (getStrLength(name) > fieldLength) {
			showAlertMask?showAlertMask({title:displayName + "长度不能超过" + fieldLength + "."}):alert(displayName + "长度不能超过" + fieldLength + ".");
			document.getElementById(fieldName).select();
			return false;
		}
	}

	return true;
}

var f = /^(([0-9]+\\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

function validFloatField(fieldName, displayName, fieldLength, allowNull) {
	var name = document.getElementById(fieldName).value;

	if (name == null || name.trim() == "") {
		if (!allowNull) {
			showAlertMask?showAlertMask({title:"请输入" + displayName + "."}):alert("请输入" + displayName + ".");
			document.getElementById(fieldName).select();
			return false;
		}
	} else {
		if (isNaN(name)) {
			showAlertMask?showAlertMask({title:displayName + "必须是数字."}):alert(displayName + "必须是数字.");
			document.getElementById(fieldName).select();
			return false;
		}
		if (getStrLength(name) > fieldLength) {
			showAlertMask?showAlertMask({title:displayName + "长度不能超过" + fieldLength + "."}):alert(displayName + "长度不能超过" + fieldLength + ".");
			document.getElementById(fieldName).select();
			return false;
		}
	}
	return true;
}

function validSpecFloatField(fieldName, displayName, fieldLength) {
	var name = document.getElementById(fieldName).value;

	if (name == null || name.trim() == "") {
		return true;
	}
	if (isNaN(name)) {
		showAlertMask?showAlertMask({title:displayName + "必须是数字."}):alert(displayName + "必须是数字.");
		document.getElementById(fieldName).select();
		return false;
	}

	if (getStrLength(name) > fieldLength) {
		showAlertMask?showAlertMask({title:displayName + "长度不能超过" + fieldLength + "."}):alert(displayName + "长度不能超过" + fieldLength + ".");
		document.getElementById(fieldName).select();
		return false;
	}

	return true;
}

function validField(fieldName, displayName, fieldLength, allowNull, defaultValue) {
	var name = document.getElementById(fieldName).value;

	if (name == null || name.trim() == "") {
		if (!allowNull) {
			showAlertMask?showAlertMask({title:"请输入" + displayName + "."}):alert("请输入" + displayName + ".");
			document.getElementById(fieldName).select();
			return false;
		}
	} else {
		if (inputTxt(name)) {
			document.getElementById(fieldName).select();
			return false;
		}
		if (getStrLength(name) > fieldLength) {
			showAlertMask?showAlertMask({title:displayName + "长度不能超过" + fieldLength + "."}):alert(displayName + "长度不能超过" + fieldLength + ".");
			document.getElementById(fieldName).select();
			return false;
		}
	}
	if (defaultValue && name == defaultValue) {
		showAlertMask?showAlertMask({title:"请输入" + displayName + "."}):alert("请输入" + displayName + ".");
		document.getElementById(fieldName).select();
		return false;
	}
	return true;
}