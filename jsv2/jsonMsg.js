var actionStr = "../jsonPortal";  
function popRequest(funId,openId,bodyStr){
	var request = {   
	    Source: 'WEB',  
	    FunctionID: funId,
	    //'ListFriends',  
	    OpenID:openId,  
	    Message: bodyStr  
	};  
	return request;
}

function popBody(funId,dataStr){
	var bodyStr = {  
	    AppID:  'System',
	    FuncID:  funId,
	    AppVersion:  'V1.00',
	    TimeStamp:  '0',
	    Data:  dataStr
	};
	return bodyStr;
}