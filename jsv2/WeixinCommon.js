wx && wx.config({
	debug: true,
	appId: "wx02d04c1b737190fe",
	timestamp: "1425463583",
	nonceStr: "6852de62",
	signature: "f2c1b68582fb2105a4b3d4305729a628105689cf",
	jsApiList: [
		'checkJsApi'
	]
});
wx && wx.error(function(res) {
	// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
	alert(JSON.stringify(res));
});

wx && wx.ready(function() {
	// config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
	//console.log(arguments);
	wx.checkJsApi({
		jsApiList: ['chooseImage'], // 需要检测的JS接口列表，所有JS接口列表见附录2,
		success: function(res) {
			// 以键值对的形式返回，可用的api值true，不可用为false
			// 如：{"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
		},
		fail: function(res) {}
	});
});
//		https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=Ac_auiVFMGTIUZvqBeYaXeP2PJKbrjN5sXwMPBsKr_BuJ3-6mpzxtTuB4fEnPusKgYll6ivWL5nvP-96LxeNBHbmDopk4IBdwZTGbidgbmY
function getSignature(appid, secret) {
	$.ajax({
		type: "get",
		url: 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + appid + '&secret=' + secret,
		async: false,
		dataType: "jsonp"
	}).done(function(res) {
		console.log('成功', res);
	}).fail(function(err) {
		console.log(err);
	});
}