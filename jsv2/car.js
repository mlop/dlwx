/**
 * 搭车操作
 */

function carFillPickupInfo(openId, pickupId, callback) {
	bidgeToWeixin(openId, 'PickupDetail', {
		PickupID: pickupId
	}, function(data, status, xhr) {
		if (data && data.portalResp && data.portalResp.Data) {
			tripLeaderId = data.portalResp.Data.OwnerId;
			receiverId = data.portalResp.Data.Receiver;
			imageURL = data.portalResp.Data.SenderPortrait;
			nickName = data.portalResp.Data.SenderName;
			$('.detailtitlebar .tripuser').attr({
				'src': imageURL
			});
			$('#owner-name').html(nickName);
			tripStatus = data.TripStatus || tripStatus;
			$('#tripStatusBar').html(["等待好友接受", "好友已接受", "搭车已结束"][parseInt(tripStatus || 0)]).removeClass('status_green greenline').addClass(["status_blue blueline", "status_green greenline", "status_gray grayline"][parseInt(tripStatus || 0)]);
			tripStatus == "2" ? changeButtonStatus() : ''; //如果已经结束就屏蔽界面按钮操作
			/*$("#mines").html("");
			$("#mines").html("<img src='" + imageURL + "'  class='user_pic'>");
			$("#nickname").html("");
			$("#nickname").html(nickName);
			$("#senderImg").attr("src", imageURL);
			$("#receiverImg").attr("src", data.portalResp.Data.ReceiverPortrait);
			$("#senderNickname").html("");
			$("#senderNickname").html(nickName);
			$("#receiverNickname").html("");
			$("#receiverNickname").html(data.portalResp.Data.ReceiverName);*/
			/*recNickname = data.portalResp.Data.ReceiverName;
			recImg = data.portalResp.Data.ReceiverPortrait;
			mapInit(new AMap.LngLat(data.portalResp.Data.Loc.Longitude / 1000000, data.portalResp.Data.Loc.Latitude / 1000000));*/
			//mapObj.setFitView();
			callback(data);
		}
	});
}