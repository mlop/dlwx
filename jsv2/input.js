function isPlaceholderSupport() {
	return 'placeholder' in document.createElement('input');
}
var s = document.getElementById("txtJourneyTitle");
s.onfocus = function() {
	if (this.value == this.defaultValue) this.value = ''
};
s.onblur = function() {
	if (/^\s*$/.test(this.value)) {
		this.value = this.defaultValue;
		this.style.color = '#999'
	}
}
s.onkeydown = function() {
	this.style.color = '#333'
}