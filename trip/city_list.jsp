<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="font-size: 8.53333333333333px;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<meta name="description" content="">
		<title>城市列表</title>
		<link href="../imgv2/css/css.css" rel="stylesheet" type="text/css">
		<style>
			body {
				background-color: rgba(255, 255, 255, 1);
			}
			.scroll-auto{
				overflow: auto;
			}
		</style>
		<script src="../jsv2/DOMContentLoaded.js"></script>
	</head>

	<body>
		<!--wrap start-->
		<section id="wrap">
			<section class="titlebar">选择城市</section>
			<div class="scroll-auto">		
			
			<section class="subtitle">热门城市</section>
			<section class="citylist whitebg lh3">
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">北京</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">上海</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">广州</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">武汉</a>
					</li>
				</ul>
			</section>
			<section class="subtitle">字母筛选</section>
			<ul class="listitem mt10">
				<a href="javascript:document.getElementById('5A').scrollIntoView();">
					<button class="sort mr10 button">A</button>
				</a>
				<a href="javascript:document.getElementById('5B').scrollIntoView();">
					<button class="sort mr10 button">B</button>
				</a>
				<a href="javascript:document.getElementById('5C').scrollIntoView();">
					<button class="sort mr10 button">C</button>
				</a>
				<a href="javascript:document.getElementById('5D').scrollIntoView();">
					<button class="sort mr10 button">D</button>
				</a>
				<a href="javascript:document.getElementById('5E').scrollIntoView();">
					<button class="sort mr10 button">E</button>
				</a>
				<!--                 <button class="sort mr10 button">A</button>
                 <button class="sort mr10 button">B</button>
                 <button class="sort mr10 button">C</button>
                 <button class="sort mr10 button">D</button>
                 <button class="sort mr10 button">E</button>                       -->
			</ul>

			<ul class="listitem mt10">
				<a href="javascript:document.getElementById('5F').scrollIntoView();">
					<button class="sort mr10 button">F</button>
				</a>
				<a href="javascript:document.getElementById('5G').scrollIntoView();">
					<button class="sort mr10 button">G</button>
				</a>
				<a href="javascript:document.getElementById('5H').scrollIntoView();">
					<button class="sort mr10 button">H</button>
				</a>
				<a href="javascript:document.getElementById('5J').scrollIntoView();">
					<button class="sort mr10 button">J</button>
				</a>
				<a href="javascript:document.getElementById('5K').scrollIntoView();">
					<button class="sort mr10 button">K</button>
				</a>
				<!--                 <button class="sort mr10 button">F</button>
                 <button class="sort mr10 button">G</button>
                 <button class="sort mr10 button">H</button>
                 <button class="sort mr10 button">J</button>
                 <button class="sort mr10 button">K</button>                       -->
			</ul>

			<ul class="listitem mt10">
				<a href="javascript:document.getElementById('5L').scrollIntoView();">
					<button class="sort mr10 button">L</button>
				</a>
				<a href="javascript:document.getElementById('5M').scrollIntoView();">
					<button class="sort mr10 button">M</button>
				</a>
				<a href="javascript:document.getElementById('5N').scrollIntoView();">
					<button class="sort mr10 button">N</button>
				</a>
				<a href="javascript:document.getElementById('5P').scrollIntoView();">
					<button class="sort mr10 button">P</button>
				</a>
				<a href="javascript:document.getElementById('5Q').scrollIntoView();">
					<button class="sort mr10 button">Q</button>
				</a>
				<!--                  <button class="sort mr10 button">L</button>
                 <button class="sort mr10 button">M</button>
                 <button class="sort mr10 button">N</button>
                 <button class="sort mr10 button">P</button>
                 <button class="sort mr10 button">Q</button>                       -->
			</ul>

			<ul class="listitem mt10">
				<a href="javascript:document.getElementById('5R').scrollIntoView();">
					<button class="sort mr10 button">R</button>
				</a>
				<a href="javascript:document.getElementById('5S').scrollIntoView();">
					<button class="sort mr10 button">S</button>
				</a>
				<a href="javascript:document.getElementById('5T').scrollIntoView();">
					<button class="sort mr10 button">T</button>
				</a>
				<a href="javascript:document.getElementById('5W').scrollIntoView();">
					<button class="sort mr10 button">W</button>
				</a>
				<a href="javascript:document.getElementById('5X').scrollIntoView();">
					<button class="sort mr10 button">X</button>
				</a>
				<!--                  <button class="sort mr10 button">R</button>
                 <button class="sort mr10 button">S</button>
                 <button class="sort mr10 button">T</button>
                 <button class="sort mr10 button">W</button>
                 <button class="sort mr10 button">X</button>                       -->
			</ul>

			<ul class="listitem mt10 mb10">
				<a href="javascript:document.getElementById('5Y').scrollIntoView();">
					<button class="sort mr10 button">Y</button>
				</a>
				<a href="javascript:document.getElementById('5Z').scrollIntoView();">
					<button class="sort mr10 button">Z</button>
				</a>
				<!--                 <button class="sort mr10 button">Y</button>
                 <button class="sort mr10 button">Z</button>                       -->
			</ul>

			<!--    <section class="subtitle">A</section>           -->
			<section class="subtitle"><a name="5A" id="5A">A</a>
			</section>
			<section class="citylist whitebg lh3">
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">阿坝</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">阿克苏</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">阿勒泰</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">阿里</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">安康</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">安庆</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">鞍山</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">安顺</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">安阳</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">澳门</a>
					</li>
				</ul>

			</section>
			<section class="subtitle"><a name="5B" id="5B">B</a>
			</section>
			<section class="citylist whitebg lh3">
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">白城</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">百色</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">白沙</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">白山</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">白银</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">保定</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">宝鸡</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">保山</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">保亭</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">包头</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">巴彦淖尔</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">巴中</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">北海</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">北京</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">蚌埠</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">本溪</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">毕节</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">滨州</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">博尔塔拉</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">亳州</a>
					</li>
				</ul>

			</section>
			<section class="subtitle"><a name="5C" id="5C">C</a>
			</section>
			<section class="citylist whitebg lh3">
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">昌江</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">沧州</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">长春</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">常德</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">昌都</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">昌吉</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">长沙</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">长治</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">常州</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">潮州</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">承德</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">成都</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">澄迈</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">郴州</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">赤峰</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">池州</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">重庆</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">崇左</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">滁州</a>
					</li>
				</ul>

			</section>
			<section class="subtitle"><a name="5D" id="5D">D</a>
			</section>
			<section class="citylist whitebg lh3">
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">大理市</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">大连</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">丹东</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">儋州</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">大庆</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">大同</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">大兴安岭</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">达州</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">德阳</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">定安</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">定西</a>
					</li>
					<li class="mtb"><a href="javascript:void(0);">东方</a>
					</li>
				</ul>
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb">东莞
						<a href="javascript:void(0);"></a>
					</li>
					<li class="cityname rightline mtb">东营
						<a href="javascript:void(0);"></a>
					</li>
				</ul>
			</section>

			<section class="subtitle"><a name="5E" id="5E">E</a>
			</section>
			<section class="citylist whitebg lh3">
				<ul class="listitem bottomline">
					<li class="cityname rightline mtb"><a href="javascript:void(0);">鄂尔多斯</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">恩施</a>
					</li>
					<li class="cityname rightline mtb"><a href="javascript:void(0);">鄂州</a>
					</li>
				</ul>

				<section class="subtitle"><a name="5F" id="5F">F</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">防城港</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">佛山</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">抚顺</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">阜新</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">阜阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">抚州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">福州</a>
						</li>
					</ul>

				</section>
				<section class="subtitle"><a name="5G" id="5G">G</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">甘南</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">赣州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">甘孜</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">广安</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">广元</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">广州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">贵港</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">桂林</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">贵阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">固原</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5H" id="5H">H</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">哈尔滨</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">海东</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">海口</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">海西</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">哈密</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">邯郸</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">杭州</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">汉中</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">鹤壁</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">河池</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">合肥</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">鹤岗</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">黑河</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">衡水</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">衡阳</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">和田</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">河源</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">菏泽</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">贺州</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">香港</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">淮安</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">淮北</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">怀化</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">淮南</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">黄冈</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">黄南</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">黄山</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">黄石</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">呼和浩特</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">惠州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">葫芦岛</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">呼伦贝尔</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">湖州</a>
						</li>
					</ul>

				</section>
				<section class="subtitle"><a name="5J" id="5J">J</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">佳木斯</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">吉安</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">江门</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">焦作</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">嘉兴</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">嘉峪关</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">揭阳</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">吉林</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">济南</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">金昌</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">晋城</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">景德镇</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">荆门</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">荆州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">金华</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">济宁</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">晋中</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">锦州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">九江</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">酒泉</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">鸡西</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">济源</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5K" id="5K">K</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">开封</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">喀什</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">克拉玛依</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">昆明</a>
						</li>
					</ul>

				</section>
				<section class="subtitle"><a name="5L" id="5L">L</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">来宾</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">莱芜</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">廊坊</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">兰州</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">拉萨</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">乐东</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">乐山</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">凉山</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">连云港</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">聊城</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">辽阳</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">辽源</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">丽江</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">临沧</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">临汾</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">临高</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">陵水</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">临夏</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">临沂</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">林芝</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">丽水</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">六盘水</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">柳州</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">陇南</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">龙岩</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">娄底</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">六安</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">漯河</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">洛阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">泸州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">吕梁</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5M" id="5M">M</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">马鞍山</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">澳门</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">茂名</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">眉山</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">梅州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">绵阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">牡丹江</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5N" id="5N">N</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">南昌</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">南充</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">南京</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">南宁</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">南平</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">南通</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">南阳</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">那曲</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">内江</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宁波</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宁德</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">怒江</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5P" id="5P">P</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">盘锦</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">攀枝花</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">澎湖</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">平顶山</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">平凉</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">屏东</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">萍乡</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">普洱</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">莆田</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">濮阳</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5Q" id="5Q">Q</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">青岛</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">庆阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">清远</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">秦皇岛</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">钦州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">琼海</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">琼中</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">齐齐哈尔</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">七台河</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">泉州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">曲靖</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">衢州</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5R" id="5R">R</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">日喀则</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">日照</a>
						</li>
					</ul>

				</section>
				<section class="subtitle"><a name="5S" id="5S">S</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">三门峡</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">三明</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">三亚</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">上海</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">商洛</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">商丘</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">山南</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">上饶</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">汕头</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">汕尾</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">韶关</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">绍兴</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">邵阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">沈阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">深圳</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">石家庄</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">十堰</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">石嘴山</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">双鸭山</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">朔州</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">四平</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">松原</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">绥化</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">遂宁</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">随州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宿迁</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宿州</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">苏州</a>
						</li>

					</ul>
				</section>

				<section class="subtitle"><a name="5T" id="5T">T</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">塔城</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">泰安</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">台北</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">太原</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">泰州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">台州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">唐山</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">天津</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">天水</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">铁岭</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">铜川</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">通化</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">通辽</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">铜陵</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">铜仁</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">吐鲁番</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">屯昌</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5W" id="5W">W</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">万宁</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">潍坊</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">威海</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">渭南</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">文昌</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">文山</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">温州</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">乌海</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">武汉</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">芜湖</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">乌兰察布</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">乌鲁木齐</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">武威</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">无锡</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">五指山</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">吴忠</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">梧州</a>
						</li>
					</ul>

				</section>
				<section class="subtitle"><a name="5X" id="5X">X</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">厦门</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">西安</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">香港</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">湘潭</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">湘西</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">襄阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">咸宁</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">咸阳</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">孝感</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">邢台</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">西宁</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">新乡</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">信阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">新余</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">忻州</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">西双版纳</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宣城</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">许昌</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">徐州</a>
						</li>
					</ul>

				</section>
				<section class="subtitle"><a name="5Y" id="5Y">Y</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">雅安</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">延安</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">盐城</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">阳江</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">阳泉</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">扬州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">烟台</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">宜宾</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宜昌</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">伊春</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">宜春</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">伊犁</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">银川</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">营口</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">鹰潭</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">益阳</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">永州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">岳阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">玉林</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">榆林</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">运城</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">云浮</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">云林</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">玉树</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">玉溪</a>
						</li>
					</ul>
				</section>

				<section class="subtitle"><a name="5Z" id="5Z">Z</a>
				</section>
				<section class="citylist whitebg lh3">
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">枣庄</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">张家界</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">张家口</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">张掖</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">漳州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">湛江</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">肇庆</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">昭通</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">朝阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">郑州</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">镇江</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">中山</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">中卫</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">周口</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">舟山</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">珠海</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">驻马店</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">株洲</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">淄博</a>
						</li>
						<li class="mtb"><a href="javascript:void(0);">自贡</a>
						</li>
					</ul>
					<ul class="listitem bottomline">
						<li class="cityname rightline mtb"><a href="javascript:void(0);">资阳</a>
						</li>
						<li class="cityname rightline mtb"><a href="javascript:void(0);">遵义</a>
						</li>
					</ul>
				</section>
				</div>
			</section>
			<!--wrap end-->

	</body>
	<script src="../jsv2/jQuery/jquery-1.11.2.min.js"></script>
	<script src="../jsv2/common.js"></script>
	<script src="../jsjspv2/journey/operateData.js"></script>
	<script src="../jsv2/journey/triplist.js"></script>
	<script>
		var parentwin = window.opener;
		$(function() {
			$(document).on('scroll', function(e) {
				var hash = window.location.hash;
				if (hash) {
					$('.subtitle:has(a[name][id])').css({
						'margin-top': '0px'
					});
					$(hash).parent().css({
						'margin-top': '51px'
					});
				}
			});
			$('.scroll-auto').css({
				'height': ($(window).height()>1000?window.screen.height- 40:$(window).height()- 30) + 'px'
			});
			$('.citylist a').on('click', function(e) {
				/*parentwin && parentwin.setIframeContentValue(parentwin,$(e.target).html());
				e.stopPropagation();
				e.preventDefault();
				window.close();
				return false;*/
				setSaveEditDataToSessionStorage('cityInfo',{city:$(e.target).html()});
				window.location.href = getSaveEditDataToSessionStorage('city_list_parent_win').url;
			});
		});
	</script>

</html>