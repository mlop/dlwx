<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="font-size: 8.53333333333333px;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<meta name="description" content="">
		<meta http-equiv="x-dns-prefetch-control" content="on">
		<title>旅程详情</title>
		<link href="../imgv2/css/css.css" rel="stylesheet" type="text/css">
		<style>
			.citylist[data-column="friendslist"] ul {
				/*height: 12rem;*/
				
				height: 7rem;
			}
			#btnChangeLeader {
				white-space: nowrap;
				text-align: center;
				width: 9rem;
			}
			.picsize > img {
				border-radius: 5px;
				height: 4.8rem;
				width: 4.8rem;
				margin: auto;
			}
			.usergroup .name {
				text-align: center;
				white-space: nowrap;
				overflow: hidden;
				text-overflow: ellipsis;
			}
			.button {
				cursor: pointer;
			}
			.button:hover {
				-webkit-border-radius: 0.5rem;
				-moz-border-radius: 0.5rem;
				border-radius: 0.5rem;
				background-color: #96cc70;
				border: solid 1px #698e4e;
				-webkit-box-shadow: inset 1px 2px 8px rgba(0, 0, 0, .33);
				-moz-box-shadow: inset 1px 2px 8px rgba(0, 0, 0, .33);
				box-shadow: inset 1px 2px 8px rgba(0, 0, 0, .33);
				color: #fff;
				font-size: 1.6rem;
				text-align: center;
			}
			.botton-disable {
				-webkit-border-radius: 0.5rem;
				-moz-border-radius: 0.5rem;
				border-radius: 0.5rem;
				background-color: #B7B7B7;
				border: solid 1px #989898;
				background-image: -webkit-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: -moz-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: -o-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: -ms-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: linear-gradient(to top, #B7B7B7, #B7B7B7);
				color: #fff;
				font-size: 1.6rem;
				text-align: center;
				cursor: default;
			}
			.botton-disable:hover {
				background-color: #96cc70;
				border: solid 1px #698e4e;
				-webkit-box-shadow: none;
				-moz-box-shadow: none;
				box-shadow: none;
			}
			.peoplenumber {
				width: 100%;
				white-space: nowrap;
			}
			.itemcon_h,
			.itemcon_h a {
				display: inline-block;
				white-space: nowrap;
			}
			.citylist .listitem.mt20 {
				margin-left: 1.2rem;
			}
			.mr15 {
				margin-right: .6rem;
			}
		</style>
		<script src="../jsv2/DOMContentLoaded.js"></script>
		<%
       if(session.getAttribute("openid_session")==null || session.getAttribute("openid_session")==""){
             String path = request.getContextPath(); 
             String qString = request.getQueryString();
             String url = request.getScheme()+"://"+ request.getServerName()+request.getRequestURI()+(qString==null?"":("?"+qString)); 
             response.sendRedirect("../oauthSessionServlet?redirectUrl="+url);
       }
  %>
	</head>
<body class="whitebg">
		<!--wrap start-->
		<section id="wrap">
			<section id="btn-share" class="black_bg" style="display: none;">
				<div class="popup_img">
					<img src="../imgv2/popup_send_friend.png" width="100%" height="20%" />
				</div>
			</section>
			<section class="black_bg alert-mask">
				<div class="w_popup_btn">
					<div class="message_text">正在加载中...</div>
					<div class="messagebtn">
						<button class="messagebtnone" style="background-color: transparent; border: 0;">确定</button>
					</div>
				</div>
			</section>
			<section class="detailtitlebar">
				<img src="" width="100%" height="100%" class="tripuser" />
				<ul class="listitem h120">
					<li class="userinfo">
						<p class="username">
							<em id="owner-name"></em> 创建的
						</p>
						<p id="tripStatusBar" class="status_green greenline">旅程进行中</p>
					</li>
					<li class="d_triptitle"><em id="trip-title" data-column="Title"></em>
					</li>
				</ul>
			</section>
			<section class="subtitle">旅程时间</section>
			<section class="citylist whitebg">
				<ul class="listitem h120 bottomline">
					<li class="infoname">
						<p class="peoplenumber">开始时间</p>
					</li>
					<li class="infocon" data-column="StartDate" id="txtJourneyStartTime"></li>
				</ul>
				<ul class="listitem h120 bottomline">
					<li class="infoname">
						<p class="peoplenumber">结束时间</p>
					</li>
					<li class="infocon" data-column="ValidDate" id="txtJourneyEndTime"></li>
				</ul>
			</section>
			<section class="subtitle">目的地</section>
			<section class="lh3 citylist whitebg">
				<ul class="listitem bottomline txtJourneyDestination">
					<li class="itemico mtb"><i class="iconfont">&#xe606;</i>
					</li>
					<li class="itemcon_h mtb">
						<a data-column="Destination" id="txtJourneyDestination" href="javascript:void(0);"></a>
					</li>
					<li class="itemarrow mtb"><i class="iconfont">&#xe60c;</i>
					</li>
				</ul>
			</section>
			<section class="citylist">
				<ul class="listitem h60 mt20">
					<button id="btnChangeJourney" class="smallbtn button left">修改旅程</button>
					<button id="btnEndJourney" class="smallbtn button right">结束旅程</button>
				</ul>
				<ul class="listitem h100 mt20">
					<li class="itemcon_h lh3 mtb"><a href="javascript:void(0);">邀请好友参加</a>
					</li>
					<li class="itemico_wechat mtb">
						<img id="onMenuShareTimeline" src="../imgv2/pengyouquan.png" class="mr30 icosize60" />
						<img id="onMenuShareAppMessage" src="../imgv2/wechat.png" class="icosize60" />
					</li>
				</ul>
			</section>
			<section class="subtitle">
				<li class="left mt10 mb10">已参加好友 <em class="friends-count">0</em>人
				</li>
				<li class="right mt10 mb10">
					<button id="btnChangeLeader" class="subbtn left mr20 button">更换领队</button>
					<button id="btnExit" class="subbtn left button">请出</button>
				</li>
			</section>
			<section class="citylist" data-column="friendslist" style="margin-bottom: 64px;">
			</section>
			<section class="bottombtnarea">
				<ul class="listitem h60">
					<button id="btnStartJourney" class="bigbtn button">
						<i class="iconfont mr10 btnico">&#xe605;</i>开始旅程
					</button>
				</ul>
			</section>
		</section>
		<!--wrap end-->
		<div class="iframe-containter"></div>
		<section class="black_bg alert-mask">
			<div class="w_popup_btn">
				<div class="message_text">正在加载中...</div>
				<div class="messagebtn">
					<button class="messagebtnone" style="background-color: transparent; border: 0;">确定</button>
				</div>
			</div>
		</section>
	</body>
	<script src="../jsv2/jQuery/jquery-1.11.2.min.js"></script>
	<!--<script src="../jsv2/common.js"></script>-->
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script src="../jsv2/lib/json3.min.js"></script>
	<script src="../jsv2/jsonMsg.js"></script>
	<script src="../jsv2/QueryStringParser.js"></script>
	<script src="../jsv2/journey/operateData.js"></script>
	<script src="../jsv2/AMapCommon.js"></script>
	<script>
		var selectUserCount = 0,
			tripData,
			openId, tripId, isActive, tripStatus, ismove = false /*好友是否删除状态*/ ;
		$(function() {
			$('#btn-share').hide().on('click', function() {
				$('#btn-share').hide();
			});
			openId = '';
			tripId = queryString.getParam("tripid");
			isActive = queryString.getParam("isActive");
			tripStatus = queryString.getParam("status");
			
			
<%if (session.getAttribute("openid_session") == null) {%>
				openId='';
				<%} else {%>
				openId = '<%=session.getAttribute("openid_session")%>';
				<%}%>
			
			if (tripId.indexOf("#") > -1) {
				tripId = tripId.replace("#", "");
			}
			/*$(document).on('click', '.usergroup', function(e) {
				if (ismove) {
					var tar = $(e.target);
					tar = _getTarparanetByClass(tar, 'usergroup');
					var istripleader = tar.attr('istripleader');
					istripleader == 'false' ? (tar.hasClass('img-selected') ? tar.removeClass('img-selected') : tar.addClass('img-selected')) : '';
				}
			});*/
			//修改旅程
			$('#btnChangeJourney').on('click', function() {
				//setIframeSrc('create_trip.html?isedit=1&isActive=' + isActive + '&openid=' + openId + '&tripid=' + tripId);
				window.location.href = 'create_trip.jsp?isedit=1&status=' + tripStatus + '&isActive=' + isActive + '&tripid=' + tripId;
			});
			$('#btnChangeLeader').on('click', function() {
				//setIframeSrc('change_leader.html?openid=' + openId + '&tripid=' + tripId);
				window.location.href = 'change_leader.jsp?isActive=' + isActive + '&tripid=' + tripId;
			});
			$('#btnEndJourney').on('click', function() {
				updateTripStatus(tripId, 'Terminate');
			});
			$('#btnStartJourney').on('click', function() {
				updateTripStatus(tripId, 'Active');
			});
			$('#btnExit').on('click', function(e) {
				var tar = $(e.target);
				if (ismove) {
					tar.html('请出');
					ismove = false;
					changeFriendsStatus();
				} else {
					tar.html('完成');
					ismove = true;
					changeFriendsStatus();
				}
			});
			$(document).on('click', 'section[data-column=friendslist] .img-selected', function(e) {
				var tar = $(this);
				tar = _getTarparanetByClass(tar, 'usergroup');
				var oid = tar.attr('OpenID');
				var dataStr = {
					TripID: tripId,
					MemberStatus: [oid],
					TripStatus: "Delete"
				};
				bidgeToWeixin(openId, 'EditTripStatus', dataStr, function(data, status, xhr) {
					tar.remove();
					retrieveTrip(tripId);
				});
			});
			$('.txtJourneyDestination').on('click', function(e) {
				if (getGEOCenter()) {
					//setIframeSrc('mapView.html');
					//openWin('mapView.html');
					setSaveEditDataToSessionStorage('mapView', {
						'url': window.location.href
					});
					window.location.href = 'mapView.jsp';
				}
			});
			$('.messagebtnone').on('click', function() {
				closeAlertMask();
			});
			if (openId == null || openId == "" || tripId == null || tripId == "") {
				//alert("参数错误...");
				showAlertMask({title:'参数错误...'});
				window.location.href = "create_trip.jsp";//?openid=" + openId;
				return false;
			} else {
				//retrieveFriends()
				//setMenu(openId, tripId);
				retrieveTrip(tripId);
			}
		});
		/**
		 * 设置清楚按钮是否可操作
		 */
		function setBtnExitCanClick() {
				if (selectUserCount > 0) {
					$('#btnExit').removeAttr('disabled').on('click', function() {});
				} else {
					$('#btnExit').off('click').attr({
						'disabled': 'disabled'
					});
				}
			}
			/**
			 * 改变好友列表状态
			 */

		function changeFriendsStatus() {
			$('section[data-column=friendslist] .usergroup').each(function() {
				var tar = $(this);
				tar = _getTarparanetByClass(tar, 'usergroup');
				var istripleader = tar.attr('istripleader');
				if (ismove) {
					istripleader == 'false' ? (tar.hasClass('img-selected') ? tar.removeClass('img-selected') : tar.addClass('img-selected')) : '';
				} else {
					tar.removeClass('img-selected')
				}
			});
		}
		var tripOwnerName = "",
			imageURL = "";

		function retrieveTrip(tripId) {
				var dataStr = {
					"TripID": tripId
				};
				bidgeToWeixin(openId, 'TripDetail', dataStr, function(rdata, status, xhr) {
					var tripMember = "",
						data = rdata && rdata.portalResp && rdata.portalResp.Data;
					tripData = data;
					$("#txtJourneyDestination").html(data.Destination.Name)
						.data('address', {
							location: {
								lat: data.Destination.Loc.Latitude / 1000000,
								lng: data.Destination.Loc.Longitude / 1000000,
							},
							name: data.Destination.Name,
							city: {
								citycode: ''
							}
						});
					setSaveEditDataToSessionStorage('tripAddress', data.Destination);
					$('.friends-count').html(data.MemberStatus.length);
					//$(".title.friends").html("<i class=\"iconfont tool_ico\">&#xe609;</i>参与人数：" + data.MemberStatus.length + "人");
					$("#txtJourneyStartTime").html((data.StartDate || data.ValidDate).replace(/年|月/g, '-').replace('日', ''));
					$("#txtJourneyEndTime").html(data.ValidDate.replace(/年|月/g, '-').replace('日', ''));
					tripStatus = data.TripStatus || tripStatus;
					$('#tripStatusBar').html(["旅程准备中", "旅程进行中", "旅程已结束"][parseInt(tripStatus || 0)]).removeClass('status_green greenline').addClass(["status_blue blueline", "status_green greenline", "status_gray grayline"][parseInt(tripStatus || 0)]);
					tripStatus == "2" ? changeButtonStatus() : ''; //如果已经结束就屏蔽界面按钮操作
					bindFriendsList(data); //绑定好友列表
					$('#trip-title').html(data.Title);
					fillWXContent(data.Title, tripOwnerName, imageURL);
					changeFriendsStatus();
				});
			}
			/**
			 * 绑定好友列表
			 * @param {Object} friends
			 */

		function bindFriendsList(data) {
			var lis = [],
				strHTML = [],
				uls = [];
			$.each(data.MemberStatus, function(index) {
				if (index % 5 == 0) {
					index > 0 && uls.length > 0 ? (uls[uls.length - 1].append(lis), lis = []) : '';
					uls.push($('<ul>').addClass('listitem mt20'));
				}
				strHTML.push('<div class="picsize">');
				//非领队才能被移除
				if (tripData.TripLeader != data.MemberStatus[index].OpenID) {
					strHTML.push('<div class="delbtn"><a href="javascript:void(0);"><img src="../imgv2/delbtn.png" width="100%" height="100%"/></a></div>');
				}
				strHTML.push('<img src="' + data.MemberStatus[index].ImageURL + '" class="img-head" /></div>');
				strHTML.push('<div class="name">' + (data.MemberStatus[index].Nickname || '') + '</div>');
				lis.push($('<div>').addClass('usergroup mr15').attr({
					'istripleader': tripData.TripLeader == data.MemberStatus[index].OpenID,
					'OpenID': data.MemberStatus[index].OpenID
				}).html(strHTML.join('')));
				strHTML = [];
				if (openId == data.MemberStatus[index].OpenID) {
					tripOwnerName = data.MemberStatus[index].Nickname;
					imageURL = data.MemberStatus[index].ImageURL;
				}
				if (data.TripLeader == data.MemberStatus[index].OpenID) {
					tripOwnerName = data.MemberStatus[index].Nickname;
					imageURL = data.MemberStatus[index].ImageURL;
					$('#owner-name').html(data.MemberStatus[index].Nickname);
					$('.detailtitlebar .tripuser').attr({
						'src': data.MemberStatus[index].ImageURL
					});
				}
			});
			if (lis.length > 0) {
				uls[uls.length - 1].append(lis);
			}
			$("section[data-column=friendslist]").empty().append(uls);
		}

		function fillWXContent(tripTitle, ownerName, imageURL) {
			var pageURL = location.href.split('#')[0];
			var dataStr = {
				PageURL: pageURL
			};
			bidgeToWeixin(openId, 'JSSignature', dataStr, function(data, status, xhr) {
				var voiceData = data && data.portalResp && data.portalResp.Data;
				if (!voiceData) {
					return false;
				}
				var timestamp = voiceData.timestamp;
				var noncestr = voiceData.noncestr;
				var signature = voiceData.signature;
				var appId = voiceData.appId;
				wx.config({
					debug: false,
					appId: appId,
					timestamp: timestamp,
					nonceStr: noncestr,
					signature: signature,
					jsApiList: [
						'hideOptionMenu',
						'showOptionMenu',
						'onMenuShareTimeline',
						'onMenuShareAppMessage'
					]
				});
				wx.ready(function() {
					wx.hideOptionMenu();
					// 2. 分享接口
					// 2.1 监听“分享给朋友”，按钮点击、自定义分享内容及分享结果接口
					document.querySelector('#onMenuShareAppMessage').onclick = function() {
						//document.getElementById("pop").style.display = "";
						$('#btn-share').show();
						wx.showOptionMenu();
						wx.onMenuShareAppMessage({
							title: '旅程邀请',
							desc: '您的好友' + ownerName + '邀请您参加' + tripTitle + '之旅。',
							//link: pageURL.substring(0, pageURL.lastIndexOf("/")) + "/trip_detail_received.html?openid=" + openId + "&acceptUser=" + openId + "&id=" + tripId + "&status=" + tripStatus,
							link: "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=" + encodeURIComponent(pageURL.substring(0, pageURL.lastIndexOf("/") - 5) + "/oauthServlet?type=2&openid=" + openId + "&tripid=" + tripId) + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect",
							imgUrl: imageURL,
							trigger: function(res) {
								//alert('用户点击发送给朋友');
							},
							success: function(res) {
								//document.getElementById("pop").style.display = "none";
								$('#btn-share').hide();
								//alert('已邀请');
							},
							cancel: function(res) {
								$('#btn-share').hide();
								//alert('已取消');
							},
							fail: function(res) {
								//alert(JSON.stringify(res));
							}
						});
						wx.onMenuShareTimeline({
							title: '邀请您参加' + tripTitle + '之旅。',
							//link: pageURL.substring(0, pageURL.lastIndexOf("/")) + "/trip_detail_received.html?openid=" + openId + "&acceptUser=" + openId + "&id=" + tripId + "&status=" + tripStatus,
							link: "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=" + encodeURIComponent(pageURL.substring(0, pageURL.lastIndexOf("/") - 5) + "/oauthServlet?type=2&openid=" + openId + "&tripid=" + tripId) + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect",
							imgUrl: imageURL,
							trigger: function(res) {
								//alert('用户点击分享到朋友圈');
							},
							success: function(res) {
								$('#btn-share').hide();
								//alert('已分享');
							},
							cancel: function(res) {
								$('#btn-share').hide();
								//alert('已取消');
							},
							fail: function(res) {
								//alert(JSON.stringify(res));
							}
						});
					};
					document.querySelector('#onMenuShareTimeline').onclick = function() {
						//document.getElementById("pop").style.display = "";
						$('#btn-share').show();
						wx.showOptionMenu();
						wx.onMenuShareAppMessage({
							title: '旅程邀请',
							desc: '您的好友' + ownerName + '邀请您参加' + tripTitle + '之旅。',
							//link: pageURL.substring(0, pageURL.lastIndexOf("/")) + "/trip_detail_received.html?openid=" + openId + "&acceptUser=" + openId + "&id=" + tripId + "&status=" + tripStatus,
							link: "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=" + encodeURIComponent(pageURL.substring(0, pageURL.lastIndexOf("/") - 5) + "/oauthServlet?type=2&openid=" + openId + "&tripid=" + tripId) + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect",
							imgUrl: imageURL,
							trigger: function(res) {
								//alert('用户点击发送给朋友');
							},
							success: function(res) {
								//document.getElementById("pop").style.display = "none";
								$('#btn-share').hide();
								//alert('已邀请');
							},
							cancel: function(res) {
								$('#btn-share').hide();
								//alert('已取消');
							},
							fail: function(res) {
								//alert(JSON.stringify(res));
							}
						});
						wx.onMenuShareTimeline({
							title: '邀请您参加' + tripTitle + '之旅。',
							//link: pageURL.substring(0, pageURL.lastIndexOf("/")) + "/trip_detail_received.html?openid=" + openId + "&acceptUser=" + openId + "&id=" + tripId + "&status=" + tripStatus,
							link: "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + appId + "&redirect_uri=" + encodeURIComponent(pageURL.substring(0, pageURL.lastIndexOf("/") - 5) + "/oauthServlet?type=2&openid=" + openId + "&tripid=" + tripId) + "&response_type=code&scope=snsapi_base&state=1#wechat_redirect",
							imgUrl: imageURL,
							trigger: function(res) {
								//alert('用户点击分享到朋友圈');
							},
							success: function(res) {
								$('#btn-share').hide();
								//alert('已分享');
							},
							cancel: function(res) {
								$('#btn-share').hide();
								//alert('已取消');
							},
							fail: function(res) {
								//alert(JSON.stringify(res));
							}
						});
					};
					var shareData = {
						title: '微信JS-SDK Demo',
						desc: '微信JS-SDK,帮助第三方为用户提供更优质的移动web服务',
						link: 'http://demo.open.weixin.qq.com/jssdk/',
						imgUrl: 'http://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRt8Qia4lv7k3M9J1SKqKCImxJCt7j9rHYicKDI45jRPBxdzdyREWnk0ia0N5TMnMfth7SdxtzMvVgXg/0'
					};
					wx.onMenuShareAppMessage(shareData);
					wx.onMenuShareTimeline(shareData);
				});
				wx.error(function(res) {
					//alert(res.errMsg);
				});
			});
		}

		function changeButtonStatus() {
			$('button[class*=button]').off('click').addClass('botton-disable');
		}

		function getGEOCenter() {
			return $('#txtJourneyDestination').data('address');
		}
	</script>

</html>