<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html style="font-size: 8.53333333333333px;">

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<meta name="description" content="">
		<meta http-equiv="x-dns-prefetch-control" content="on">
		<title>搭车</title>
		<link href="../imgv2/css/css.css" rel="stylesheet" type="text/css">
		<style>
			#tripList {
				overflow-y: auto;
				margin-bottom: 5px;
			}
			.listitem {
				margin-top: 1.25rem;
			}
			.bottom-btn .listitem{
				margin-top: 0px;
			}
		</style>
		<script src="../jsv2/DOMContentLoaded.js"></script>
		<% if(session.getAttribute( "openid_session")==null || session.getAttribute( "openid_session")=="" ){ 
			String path=request.getContextPath(); 
			String qString=request.getQueryString(); 
			String url=request.getScheme()+ "://"+ request.getServerName()+request.getRequestURI()+(qString==null?"":( "?"+qString)); response.sendRedirect( "../oauthSessionServlet?redirectUrl="+url); }
		%>
	</head>

	<body class="whitebg">
		<!--wrap start-->
		<section id="wrap">
			<section class="tabarea">
				<ul class="tabbg">
					<li class="tabbtn tabbtn_active" listtype="3" ordertype="1"><a href="javascript:void(0);">我的请求</a>
					</li>
					<li class="tabbtn" listtype="3" ordertype="2"><a href="javascript:void(0);">好友的请求</a>
					</li>
				</ul>
			</section>
			<section id="contain" class="citylist whitebg">
				<!--<a href="javascript:void(0);">
					<img src="../imgv2/user_pic.png" width="100%" height="100%" class="tripuser" />
					<ul class="listitem h120 bottomline">
						<li class="triptitle mt10">发送给 好友 的请求</li>
						<li class="tripinfo">
							<p class="peoplenumber"></p>
							<p class="status_green w160">等待好友接受</p>
						</li>
					</ul>
				</a>
				<a href="javascript:void(0);">
					<img src="../imgv2/user_pic.png" width="100%" height="100%" class="tripuser" />
					<ul class="listitem h120 bottomline">
						<div class="triptitle mt10">发送给Gavin的请求</div>
						<div class="tripinfo">
							<p class="peoplenumber"></p>
							<p class="status_green w160">好友已接受</p>
						</div>
					</ul>
				</a>
				<a href="javascript:void(0);">
					<img src="../imgv2/user_pic.png" width="100%" height="100%" class="tripuser" />
					<ul class="listitem h120 bottomline">
						<div class="triptitle mt10">发送给Jim的请求</div>
						<div class="tripinfo">
							<p class="peoplenumber"></p>
							<p class="status_gray w160">好友已拒绝</p>
						</div>
					</ul>
				</a>
				<a href="javascript:void(0);">
					<img src="../imgv2/user_pic.png" width="100%" height="100%" class="tripuser" />
					<ul class="listitem h120 bottomline">
						<div class="triptitle mt10">发送给Jim的请求</div>
						<div class="tripinfo">
							<p class="peoplenumber"></p>
							<p class="status_gray w160">搭车已结束</p>
						</div>
					</ul>
				</a>-->
			</section>
			<section class="bottombtnarea bottom-btn">
				<ul class="listitem h60">
					<button id="btn-more" class="bigbtn button" onClick="this.className='button_d bigbtn'" onmouseout="this.className='bigbtn button'">获取更多</button>
				</ul>
			</section>
		</section>
	</body>
	<script src="../jsv2/jQuery/jquery-1.11.2.min.js"></script>
	<script src="../jsv2/lib/json3.min.js"></script>
	<script src="../jsv2/jsonMsg.js"></script>
	<script src="../jsv2/QueryStringParser.js"></script>
	<script src="../jsv2/journey/operateData.js"></script>
	<!--<script src="../jsv2/common.js"></script>-->
	<script>
		var latLon = "";
		var openId = '';
		var innerContain = "";
		var currentListType = 3;
		var currentOrderType = 1;
		<%if (session.getAttribute("openid_session") == null) {%>
		openId='';
		<%} else {%>
		openId = '<%=session.getAttribute("openid_session")%>';
		<%}%>

		function getItem(url, title, senderImage, receiverImage, status, receiver,time) {
			var content = [];
			var color = "green";
			var rightContent = "好友已接受";
			if (status == '1' || status == undefined) {
				color = "green";
				rightContent = "好友已接受";
			} else if (status == '0') {
				color = "blue";
				rightContent = "等待好友接受";
			} else {
				color = "gray";
				rightContent = "搭车已结束";
			}
			/*content += '<a href="' + url + '">';
			content += '<div class="list_group">';
			content += '<img src="' + senderImage + '" width="50px;" height="50px;" class="user_pic">';
			content += '<div class="list_text">';
			content += '<div class="list_title_car">' + title + '</div>';
			content += '<div class="list_car_to">';
			content += '<p class="fl mr10"><img src="../imgv2/green_arrow.png"></p>';
			content += '<p class="fl"><img src="' + receiverImage + '" width="50px;" height="50px;"></p>';
			content += '</div>';
			content += '</div>';
			content += '</div>';
			content += '</a>';*/
			content.push('<a href="' + (url || 'javascript:void(0);') + '">');
			content.push('	<img src="' + senderImage + '" width="100%" height="100%" class="tripuser" />');
			content.push('  <ul class = "listitem h100 bottomline">');
			content.push('		<div class="triptitle">' + title + '</div>');
			content.push('		<div class="triptitle mt10">' + time + '</div>');
			//content.push(' 		<div class = "tripinfo">');
			//content.push('			<p class="peoplenumber"></p>');
			//content.push(' 			<p class = "status_' + color + ' w160" >' + rightContent + '</p>');
			content.push('		</div>');
			content.push('	</ul>');
			content.push('</a>');
			return content.join('');
		}

		function handleOrder(orderType) {
				if (openId == null || openId == "") {
					return false;
				} else {
					$('.sort').find('span').removeClass('sortstyle_yellow2');
					if (orderType == 1) {
						$('.sort').find('span:first').addClass('sortstyle_yellow2');
					} else if (orderType == 2) {
						$('.sort').children('div').eq(1).find('span').addClass('sortstyle_yellow2');
					}
					currentOrderType = orderType;
					sendRequest(currentListType, currentOrderType, 0, 15);
				}
			}
			/*function handleTab(listType) {
				if (openId == null || openId == "") {
					return;
				}
				$('.tab3').find('div').removeClass('tabstyle2_d').removeClass('tabstyle2').removeClass('line_left');
				if (listType == 2) {
					$('.tab3').children('div').eq(1).addClass('tabstyle2_d');
					$('.tab3').find('div:first').addClass('tabstyle2').addClass('line_left');
				} else {
					$('.tab3').children('div').eq(1).addClass('tabstyle2').addClass('line_left');
					$('.tab3').find('div:first').addClass('tabstyle2_d');
				}
				currentListType = listType;
				currentOrderType = 1;
				handleOrder(currentOrderType);
			}*/

		function sendRequest(listType, orderType, startCount, limitCount) {
			var dataStr = {
				ListType: listType,
				OrderType: orderType,
				Cursor: startCount,
				Limit: limitCount,
				Direction: 0
			};
			bidgeToWeixin(openId, 'ListPickup', dataStr, function(data, status, xhr) {
				//$("#contain").html('');
				if (data.portalResp.Data.pickups && data.portalResp.Data.pickups.length > 0) {
					var content = "",
						pickups = data.portalResp.Data.pickups;
						console.log(data);
					$.each(pickups, function(index, item) {
						var url = 'car_waiting.jsp?pickupid=' + item.PickupID;
						var title = "";
						if (orderType == 1) {
							//url = (item.Receiver == "0" ? 'car_waiting' : 'navi_map_car') + '.html?openid=' + openId + '&pickupid=' + item.PickupID;
							url ='car_waiting.jsp?pickupid=' + item.PickupID;
							title = '发送给 ' + (item.Receiver == "0" ? '好友' : item.ReceiverNickName) + ' 的请求';
						} else {
							url = 'navi_map_car.jsp?pickupid=' + item.PickupID;
							title = (item.Receiver == "0" ? '好友' : item.SenderNickName) + ' 发送给您的请求';
						}
						content += getItem(url, title, item.SenderPortrait, item.ReceiverPortrait, item.status, item.Receiver,item.CreateTime);
						//item.Receiver == '0' ? '' : getUserInfo(orderType==1?item.Receiver:item.OwnerId);
					});
					if (startCount == 0) {
						//innerContain.push('<a class="btn-more" listtype="' + listType + '" ordertype="' + orderType + '" startcount="' + (startCount+limitCount) + '" limitcount="' + limitCount + '" href="javascript:void(0);"><div>更多<i class="icon iconfont">&#xe600;</i></div></a>');
						$("#contain").html(content);
					} else {
						//$(innerContain.join('')).insertBefore($('.btn-more'));
						$("#contain").append(content);
					}
					pickups.length < limitCount ? $('.bottom-btn').hide() : $('.bottom-btn').show();
					pickups.length < limitCount ? '' : $("#contain").css({
						'margin-bottom': '5.6rem'
					});
					$('#btn-more').attr({
						listtype: listType,
						ordertype: orderType,
						startcount: (startCount + limitCount),
						limitcount: limitCount
					});
				} else {
					startCount == 0 ? $("#contain").html("") : '';
					$('.bottom-btn').hide();
				}
			});
		}
		$(document).ready(function() {
			if (openId.indexOf("#") > -1) {
				openId = openId.replace("#", "");
			}
			$('.tabbg li').on('click', function(e) {
				var tar = $(e.target),
					tar = _getTarparanet(tar, {
						key: 'tagName',
						'value': 'LI'
					}),
					listtype = parseInt(tar.attr('listtype')),
					ordertype = parseInt(tar.attr('ordertype'));
				handleOrder(ordertype);
				$('.tabbg li').removeClass('tabbtn_active');
				tar.addClass('tabbtn_active');
			});
			//添加更多数据
			$(document).on('click', '#btn-more', function(e) {
				var tar = $(e.target);
				sendRequest(parseInt(tar.attr('listtype')), parseInt(tar.attr('ordertype')), parseInt(tar.attr('startcount')), parseInt(tar.attr('limitcount')));
			});
			handleOrder(currentOrderType);
		});

		function getUserInfo(oid) {
			bidgeToWeixin(openId, 'UserDetail', {
				OpenID: oid
			}, function(data, status, xhr) {
				if (data && data.portalResp && data.portalResp.Data && data.portalResp.Data.Profiles) {
					$('em[openid="' + data.portalResp.Data.Profiles.OpenID + '"]').html(data.portalResp.Data.Profiles.NickName);
				}
			});
		}
	</script>

</html>