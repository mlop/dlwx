<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="font-size: 8.53333333333333px;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<meta name="description" content="">
		<meta http-equiv="x-dns-prefetch-control" content="on">
		<title>搭车</title>
		<link href="../imgv2/css/css.css" rel="stylesheet" type="text/css">
		<style>
			#iCenter {
				width: 100%;
				height: 100%;
			}
			.d_triptitle {
				display: inline-block;
				white-space: nowrap;
				overflow: hidden;
				text-overflow: ellipsis;
			}
			.botton-disable {
				-webkit-border-radius: 0.5rem;
				-moz-border-radius: 0.5rem;
				border-radius: 0.5rem;
				background-color: #B7B7B7;
				border: solid 1px #989898;
				background-image: -webkit-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: -moz-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: -o-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: -ms-linear-gradient(bottom, #B7B7B7, #B7B7B7);
				background-image: linear-gradient(to top, #B7B7B7, #B7B7B7);
				color: #fff;
				font-size: 1.6rem;
				text-align: center;
				cursor: default;
			}
			.botton-disable:hover {
				background-color: #96cc70;
				border: solid 1px #698e4e;
				-webkit-box-shadow: none;
				-moz-box-shadow: none;
				box-shadow: none;
			}
			.user_popup {
				height: 4rem;
			}
			.user_popup .user_img {
				width: 64px;
				padding: 6px;
				text-align: left;
				margin-left: 0px;
				padding-left:0px;
				padding-right: 0px;
				border-radius: 8px;
				margin-bottom: 0px;
				box-shadow: 1px 1px 1px rgba(222, 222, 222, 1), -1px -1px 1px rgba(222, 222, 222, 1);
				border: 1px solid rgba(222, 222, 222, 1);
			}
			.user_popup .user_img:after {
				content: '';
				width: 12px;
				height: 12px;
				margin-left: 25px;
				margin-right: auto;
				margin-top: 0px;
				position: absolute;
				-moz-transform: rotate(45deg);
				-webkit-transform: rotate(45deg);
				-o-transform: rotate(45deg);
				transform: rotate(45deg);
			}
			.user_popup .downarrow {
				text-align: center;
			}
			.user_popup .downarrow img {
				margin: auto;
			}
			.user_img p {
				text-align: center;
				white-space: nowrap;
				overflow: hidden;
				text-overflow: ellipsis;
				font-size: 12px;
				margin: 0px;
			}
			.user_img .user_popup_name {
				margin-bottom: 4px;
				margin-top: 0px;
			}
			.user_img_blue,
			.user_img_blue:after {
				background-color: rgba(41, 163, 204, 1);
				color: rgba(255, 255, 255, 1);
			}
			.user_img_normal,
			.user_img_normal:after {
				background-color: rgba(255, 255, 255, 1);
			}
			.current-location {
				position: absolute;
				left: 10px;
				bottom: 100px;
				cursor: pointer;
			}
			.current-location i {
				font-size: 3rem;
				color: rgba(23, 155, 200, 1);
			}
			.amap-zoom-touch-minus {
				bottom: -48px;
			}
			.amap-zoom-touch-plus {
				bottom: 0px;
			}
			.black_bg {
				height: 100%;
			}
			.username {
  font-size: 14px;
}
		</style>
		<script src="../jsv2/DOMContentLoaded.js"></script>
	</head>
<body class="whitebg">
		<!--wrap start-->
		<section id="wrap">
			<section id="wx-add" class="black_bg" style="display: none;">
				<div class="popup_img">
					<img src="../imgv2/popup_tishi_car.png" width="100%" height="20%" />
				</div>
			</section>
			<section id="wx-share" class="black_bg" style="display: none;">
				<div class="popup_img">
					<img src="../imgv2/popup_send_friend_car.png" width="100%" height="20%" />
				</div>
			</section>
			<section id="user-address" class="black_bg" style="display: none;">
				<div class="w_popup">正在获取当前位置...</div>
			</section>
			<section class="detailtitlebar">
				<img id="mines" src="" width="100%" height="100%" class="tripuser" />
				<ul class="listitem h120">
					<li class="userinfo">
						<p class="username">当前位置</p>
					</li>
					<li class="d_triptitle"><i class="iconfont mr10">&#xe606;</i><em id="trip-title"><!--<img src="../imgv2/loading.gif" class="loading" />--></em>
					</li>
				</ul>
			</section>
			<section>
				<div id="iCenter"></div>
			</section>
			<section class="bottombtnarea">
				<ul class="listitem h60">
					<button id="btnSendePositionToFriends" class="bigbtn button botton-disable" onClick="this.className='button_d bigbtn botton-disable'" onmouseout="this.className='bigbtn button botton-disable'">发送好友位置给车载导航</button>
				</ul>
			</section>
		</section>
		<!--wrap end-->
		<input type="hidden" id="txtJourneyDestination" />
	</body>
	<script src="../jsv2/jquery-1.10.1.min.js"></script>
	<!--<script src="../jsv2/common.js"></script>-->
	<script src="http://webapi.amap.com/maps?v=1.3&key=57c58c7b1f94514317df97e3a985962c"></script>
	<script src="../jsv2/lib/json3.min.js"></script>
	<script src="../jsv2/PageValueValidate.js"></script>
	<script src="../jsv2/lib/QueryStringParser.js"></script>
	<script src="../jsv2/jsonMsg.js"></script>
	<script src="../jsv2/lib/k-utils-fn.js"></script>
	<script src="../jsv2/journey/operateData.js"></script>
	<script>
		var mapObj, windowsArr = [],
			marker = [],
			markers = [],
			latLon = "",
			senderUser=queryString.getParam("senderUser"),
			openId = '',
			pickupId = queryString.getParam('pickupid'),
			isJoined = queryString.getParam('isJoined'),
			nickName,
			pickupStatus = '4',
			imageURL, geolocation;


<%if (session.getAttribute("openid_session") == null) {%>
				openId='';
				<%} else {%>
				openId = '<%=session.getAttribute("openid_session")%>';
				<%}%>
				
		function setLocation() {
			if (geolocation) {
				geolocation.getCurrentPosition();
				//geolocation.watchPosition();
			} else {
				setTimeout(setLocation, 1000);
			}
		}

		function geoInit(isCenter) {
				geolocation = new AMap.Geolocation({
					enableHighAccuracy: true, //是否使用高精度定位，默认:true  
					timeout: 100000, //超过10秒后停止定位，默认：无穷大  
					maximumAge: 0, //定位结果缓存0毫秒，默认：0  
					convert: true, //自动偏移坐标，偏移后的坐标为高德坐标，默认：true  
					showButton: false, //显示定位按钮，默认：true  
					buttonPosition: 'LB', //定位按钮停靠位置，默认：'LB'，左下角  
					buttonOffset: new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)  
					showMarker: false, //定位成功后在定位到的位置显示点标记，默认：true  
					markerOptions: {
						title: "locationIMG",
						offset: {
							x: -10,
							y: 20
						},
						icon: new AMap.Icon({
							image: "http://webapi.amap.com/theme/v1.2/my_location.png",
							size: new AMap.Size(16, 16),
							imageOffset: {
								x: -24,
								y: -2
							}
						})
					},
					showCircle: false, //定位成功后用圆圈表示定位精度范围，默认：true  
					panToLocation: isCenter, //定位成功后将定位到的位置作为地图中心点，默认：true  
					zoomToAccuracy: true //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false  
				});
				mapObj.addControl(geolocation);
				geoSuccessListener = AMap.event.addListener(geolocation, 'complete', OnComplete); //返回定位信息,初始时定位后设为地图中心点  
				geoErrorListener = AMap.event.addListener(geolocation, 'error', onError); //返回定位出错信息  
			}
			/* 
			 *解析定位错误信息
			 */

		function onError(data) {
			$('#user-address').hide();
			var str = '定位失败,';
			str += '错误信息：'
			switch (data.info) {
				case 'PERMISSION_DENIED':
					str += '浏览器阻止了定位操作';
					break;
				case 'POSITION_UNAVAILBLE':
					str += '无法获得当前位置';
					break;
				case 'TIMEOUT':
					str += '定位超时';
					break;
				default:
					str += '未知错误';
					break;
			}
			setLocation();
		};

		function geoRemove() {
			mapObj.removeControl(geolocation);
			AMap.event.removeListener(geoSuccessListener);
			AMap.event.removeListener(geoErrorListener);
		}
		var _lat, _lng;

		function OnComplete(data) {
				/*_lng = data.position.getLng();
				_lat = data.position.getLat();
				//lnglatXY = new AMap.LngLat(data.position.getLng(), data.position.getLat());
				//var nickName = $('#nickname').text();
				//var imageURL = $('#mines').find('img').attr('src');
				geocoder(_lng, _lat);
				setMemberLocation(openId, data.position.getLng(), data.position.getLat(), imageURL, '1', 20, nickName);*/
			}
			/**
			 * 直接调用详细数据中的经纬度
			 * @param {Object} lat
			 * @param {Object} lng
			 */

		function initOnComplete(lat, lng, img, name) {
			_lng = lng;
			_lat = lat;
				//lnglatXY = new AMap.LngLat(data.position.getLng(), data.position.getLat());
				//var nickName = $('#nickname').text();
				//var imageURL = $('#mines').find('img').attr('src');
			geocoder(_lng, _lat);
			setMemberLocation(senderUser, lng, lat, img, '1', 20, name);
		}
		var lnglatXY = null;
		var destinationName = "";
		 //已知点坐标
		function geocoder(lng, lat) {
			var MGeocoder;
			//加载地理编码插件
			AMap.service(["AMap.Geocoder"], function() {
				MGeocoder = new AMap.Geocoder({
					radius: 1000,
					extensions: "all"
				});
				//逆地理编码
				MGeocoder.getAddress(new AMap.LngLat(lng, lat), function(status, result) {
					$('#user-address').hide();
					if (parseInt(pickupStatus) ==0) { //状态小于4，说明还没有被接受
						$('#btnSendePositionToFriends').removeClass('botton-disable').attr({
							"onClick": "this.className='button_d bigbtn'",
							"onmouseout": "this.className='bigbtn button'"
						});
					}
					if (status === 'complete' && result.info === 'OK') {
						$('#txtJourneyDestination').data('address', {
							location: {
								lat: lat,
								lng: lng,
							},
							name: result.regeocode.formattedAddress,
							city: {
								citycode: result.regeocode.addressComponent.citycode
							}
						});
						$('#trip-title').attr({
							'title': result.regeocode.formattedAddress
						}).html(result.regeocode.formattedAddress);
					}
				});
			});
		}

		function setMemberLocation(_oID, _lng, _lat, _imageUrl, _isLeader, currentMember, nickNameIn, deepstatus) {
			var _m = getExistsInMarkers(_oID);
			_m ? _m.setMap(null) : '';
			deepstatus = (deepstatus ? deepstatus : 'mrn') == 'mrn' ? 'pin_car' : 'pin_phone';
			if (_m) {
				if (currentMember > 1) {
					_m.setzIndex(currentMember);
				}
				_m.setPosition(new AMap.LngLat(_lng, _lat));
				_m.setMap(mapObj);
			} else {
				var marker = new AMap.Marker({
					position: new AMap.LngLat(_lng, _lat),
					offset: {
						x: -25,
						y: -72
					},
					title: _oID
				});
				//自定义点标记内容     
				var markerContent = document.createElement("div");
				markerContent.className = "user_popup";
				//头像
				var markerContentDiv2 = document.createElement("div");
				if (_isLeader) {
					markerContentDiv2.className = "user_img user_img_blue";
				} else {
					markerContentDiv2.className = "user_img user_img_normal";
				}
				var markerP2 = document.createElement("p");
				var markerImg2 = document.createElement("img");
				markerImg2.src = _imageUrl !== null && _imageUrl !== "" && _imageUrl != undefined ? _imageUrl : "../imgv2/friend_pic.png";
				markerImg2.width = 50;
				markerImg2.height = 50;
				markerP2.appendChild(markerImg2);
				markerContentDiv2.appendChild(markerP2);
				markerContent.appendChild(markerContentDiv2);
				//向下的箭头不需要
				var markerContentDiv3 = document.createElement("div");
				markerContentDiv3.className = "downarrow";
				/*var markerImg3 = document.createElement("img");
				markerImg3.src = '../imgv2/' + deepstatus + '.png';
				markerContentDiv3.appendChild(markerImg3);
				markerContent.appendChild(markerContentDiv3);*/
				if (currentMember > 1) {
					marker.setzIndex(currentMember);
				}
				//marker.setTitle(_oID)
				marker.setContent(markerContent); //更新点标记内容
				//marker.setPosition(new AMap.LngLat(_lng, _lat)); //更新点标记位置
				marker.setMap(mapObj);
				//mapObj.setZoomAndCenter(14, new AMap.LngLat(_lng, _lat));
				//判断是否自动调整地图位置//根据5.20修改意见，每次更新地图后需要将所有人员拉动到视图范围内
				//isDragstart ? '' : mapObj.setFitView();
				mapObj.setFitView()
				markers.push(marker);
			}
		}

		function getExistsInMarkers(titleOpenID) {
			for (var i = 0; i < markers.length; i++) {
				var m = markers[i];
				if (m.getTitle() === titleOpenID) {
					return m;
				}
			}
			return null;
		}

		function mapInit(center) {
			mapObj = new AMap.Map('iCenter', {
				level: 15
			});
			mapObj.plugin(["AMap.ToolBar"], function() {
				toolBar = new AMap.ToolBar();
				mapObj.addControl(toolBar);
			});
			mapObj.plugin('AMap.Geolocation', function() {
				geoInit(false);
			});
		};
		$(function() {
			if (openId == null || openId == "") {
				return false;
			} else {
				if(isJoined!="1"){
					$('#wx-add').show();
				}
				$('#iCenter').css({
					'height': (K.getPageSize()[1] - 60) + 'px'
				});
				/*var dataStr = {
					OpenID: openId + ""
				};
				bidgeToWeixin(openId, 'UserDetail', dataStr, function(data, status, xhr) {
					var url = data.portalResp.Data.Profiles.ImageURL;
					console.log(url);
					if (url.substring(url.length - 3) == "/64") {
						url = url.substring(0, url.length - 3) + "/46";
					}
					console.log(url);
					$("#mines").attr({
						'src': url
					});
					$(".username").html(data.portalResp.Data.Profiles.NickName + ' 当前位置');
					imageURL = url;
					nickName = data.portalResp.Data.Profiles.NickName;
				});*/
			}
			//$('#user-address').show();
			getPickupDetail();
		});
		 //用于获取搭车请求的状态,用于标注底部按钮是否可用
		function getPickupDetail() {
			bidgeToWeixin(senderUser, 'PickupDetail', {
				PickupID: pickupId
			}, function(data, status, xhr) {
				if (data) {
					pickupStatus = parseInt(data.portalResp.Data.Status);
					/*if(pickupStatus==4){
						window.location.href = 'navi_map_car.html?openid=' + openId + '&pickupid=' + pickupId;
					}*/
				}
				$("#mines").attr({
					'src': data.portalResp.Data.SenderPortrait
				});
				$(".username").html(data.portalResp.Data.SenderName + ' 当前位置');
				imageURL = data.portalResp.Data.SenderPortrait;
				nickName = data.portalResp.Data.SenderName;
				
				mapInit(new AMap.LngLat(data.portalResp.Data.Loc.Longitude / 1000000, data.portalResp.Data.Loc.Latitude / 1000000));
				initOnComplete(data.portalResp.Data.Loc.Latitude / 1000000, data.portalResp.Data.Loc.Longitude / 1000000, data.portalResp.Data.SenderPortrait, data.portalResp.Data.SenderName);
				mapObj.setZoomAndCenter(15, new AMap.LngLat(data.portalResp.Data.Loc.Longitude / 1000000,
					data.portalResp.Data.Loc.Latitude / 1000000));

				if (parseInt(pickupStatus) == 0) { //状态为未接受，就可以进行操作
					$('#btnSendePositionToFriends').on('click', function() {
						var loc = {
							Latitude: 29579916,
							Longitude: 11598905
						};
						loc.Latitude = parseFloat(String(_lat)) * 1000000;
						loc.Longitude = parseFloat(String(_lng)) * 1000000;
						var dataStr = {
							"PickupID": pickupId,
							"Status": '4',
							"Loc": loc
						};
						bidgeToWeixin(openId, 'UpdateStatusPickup', dataStr, function(data, status, xhr) {
							window.location.href = 'navi_map_car.jsp?pickupid=' + pickupId;
						});
					});
				}
			});
		}

		function getGEOCenter() {
			return $('#txtJourneyDestination').data('address');
		}
		//setLocation();
	</script>

</html>