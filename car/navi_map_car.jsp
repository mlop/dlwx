<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html style="font-size: 8.53333333333333px;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no">
		<meta name="description" content="">
		<meta http-equiv="x-dns-prefetch-control" content="on">
		<title>搭车</title>
		<!--<link rel="stylesheet" href="../imgv2/css/default.min.css" />-->
		<link href="../imgv2/css/css.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="../imgv2/icon/iconfont.css" />
		<style>
			body {
				font-size: 14px;
			}
			#wrap {
				max-width: 100%;
			}
			/*.navi_titlebar {
				background-color: rgba(150, 204, 112, 1);
				position: fixed;
				width: 100%;
				top: 0px;
				left: 0px;
				z-index: 200;
				padding-left: .3rem;
			}
			.navi_titlebar .userinfo {
				top: 0px;
			}
			.navi_titlebar .d_triptitle {
				font-size: 1.5rem;
				top: 3rem;
				text-indent: 0px;
				overflow: hidden;
				white-space: nowrap;
				text-overflow: ellipsis;
			}
			.navi_titlebar .h120 {
				height: 5.5rem;
			}
			.tripuser_navi {
				margin-top: .25rem;
				margin-bottom: .25rem;
				margin-right: 0.5rem;
			}*/
			.btns-info {
				top: 144px;
				height: 22px;
				width: 100%;
				text-align: center;
				background-color: rgba(255, 255, 255, 0);
				margin: auto;
				transition: All 0.4s ease-in-out;
				-webkit-transition: All 0.4s ease-in-out;
				-moz-transition: All 0.4s ease-in-out;
				-o-transition: All 0.4s ease-in-out;
			}
			.btns-info.hide {
				top: 36px;
			}
			.btns-info i {
				display: inline-block;
				width: 30%;
				background-color: rgba(150, 204, 112, 1);
				line-height: 22px;
				top: 0px;
				border-bottom-left-radius: 5px;
				border-bottom-right-radius: 5px;
			}
			.photo-img {
				height: 40px;
				width: 40px;
				margin: 0px;
				border-radius: 3px;
				margin-left: 3px;
				margin-right: 3px;
				margin-top: 4px;
			}
			.address-info>table {
				width: 100%;
				height: inherit;
				display: inherit;
			}
			.address-info tr {
				height: 24px;
				line-height: 24px;
			}
			.address-info td {
				height: inherit;
				line-height: inherit;
			}
			.td1 {
				width: 20%;
			}
			.td2 {
				width: 60%;
			}
			.time-info {
				border-top: 1px solid rgba(255, 255, 255, .5);
				border-bottom: 1px solid rgba(255, 255, 255, .5);
			}
			.time-info>div {
				float: left;
				width: 50%;
				text-align: left;
				height: 38px;
				margin-top: 5px;
				margin-bottom: 5px;
			}
			.time-info span,
			.location-info span {
				display: inline-block;
				width: 100%;
				line-height: 19px;
			}
			.endtime {
				border-left: 1px solid rgba(255, 255, 255, 1);
				padding-left: 10px;
			}
			.label-title {
				font-size: 12px;
			}
			.has-hide {
				display: none;
			}
			#iCenter {
				height: 100%;
				width: 100%;
				/*margin-top: 36px;*/
			}
			.location-info.hide {
				height: 36px;
			}
			.location-info.hide .label-title {
				display: none;
			}
			.location-info.hide span {
				line-height: 36px;
			}
			.btns-bottom-list {
				height: 36px;
				position: fixed;
				left: 3%;
				bottom: 10px;
				background-color: rgba(255, 255, 255, 1);
				width: 92%;
				padding: 3px;
				box-shadow: 1px 1px 2px rgba(222, 222, 222, 1), -1px -1px 2px rgba(222, 222, 222, 1);
				border-radius: 3px;
				z-index: 999;
				/*border: 1px solid rgba(222,222,222,1);*/
			}
			.btns-bottom-list span,
			.btns-item-soundrecording em {
				display: inline-block;
				width: 28%;
				line-height: 36px;
				text-align: center;
				border-radius: 3px;
				white-space: nowrap;
			}
			.btns-bottom-list .btns-item-soundrecording {
				width: 40%;
			}
			.btns-item-friend.selected,
			.btns-item-soundrecording.selected em,
			.btns-item-conversation.selected,
			.btns-item-friend:hover,
			.btns-item-soundrecording em:hover,
			.btns-item-conversation:hover {
				background-color: rgba(150, 204, 112, 1);
			}
			.btns-item-conversation.new-conversation.has-new:after {
				content: '';
				width: 10px;
				height: 10px;
				background-color: rgba(255, 0, 0, 1);
				border-radius: 50%;
				position: absolute;
				margin-top: 5px;
				margin-left: 5px;
			}
			.btns-item-soundrecording {
				border-left: 1px solid rgba(222, 222, 222, 1);
				border-right: 1px solid rgba(222, 222, 222, 1);
			}
			.btns-item-soundrecording em {
				display: inherit;
				height: inherit;
				line-height: inherit;
				width: 92%;
			}
			/**下面弹出文本**/
			
			.up-list {
				height: 0%;
				width: 100%;
				position: fixed;
				z-index: 9999;
				bottom: 0%;
				left: 0%;
				background-color: rgba(255, 255, 255, 1);
				padding: 0px;
				transition: All 0.4s ease-in-out;
				-webkit-transition: All 0.4s ease-in-out;
				-moz-transition: All 0.4s ease-in-out;
				-o-transition: All 0.4s ease-in-out;
			}
			.up-list-title {
				height: 28px;
				background-color: rgba(150, 204, 112, 1);
			}
			.up-list-title span {
				display: inline-block;
				width: 48%;
				float: left;
				line-height: 28px;
			}
			.up-list-title-txt {
				text-indent: 1em;
				color: rgba(255, 255, 255, 1);
			}
			.up-list-title-close {
				text-align: right;
				padding-right: 10px;
				cursor: pointer;
				color: rgba(255, 255, 255, 1);
			}
			.up-list-content {
				background-color: rgba(255, 255, 255, 1);
				overflow: hidden;
				overflow-y: auto;
				height: 90%;
				width:95%;
				padding-left: 10px;
				padding-right: 10px;
			}
			.new-conversation-status .call-item {
				height: 48px;
				line-height: 32px;
				width: 96%;
				text-align: left;
				display: inline-block;
			}
			.new-conversation-status .call-item .header-img {
				width: 36px;
			}
			.new-conversation-status .call-item:first-child {
				margin-top: 8px;
			}
			.new-conversation-status .call-item.right {
				text-align: right;
				margin-left: -10px;
			}
			.new-conversation-status .call-item img {
				height: 28px;
				width: 28px;
				line-height: 28px;
				border-radius: 3px;
				vertical-align: middle;
			}
			.new-conversation-status .call-item span {
				display: inline-block;
				line-height: 28px;
				height: 28px;
				width: 40%;
				float: left;
			}
			.new-conversation-status .call-item.right span {
				float: right;
			}
			.new-conversation-status .call-item .time {
				color: rgba(156, 156, 156, 1);
				font-size: 12px;
				text-indent: 1em;
				width: 30%;
			}
			.video {
				background-color: rgba(241, 241, 241, 1);
				margin-left: 10px;
				border-radius: 3px;
			}
			.video i {
				display: inline-block;
				width: 45%;
				height: inherit;
				background-repeat: no-repeat;
			}
			.video .video-text {
				padding-left: 10px;
				color: rgba(0, 0, 0, 1);
			}
			.video .video-btn {
				color: rgba(0, 0, 0, 1);
				text-align: right;
			}
			.new-conversation-status .call-item.right .video-text {
				color: rgba(255, 255, 255, 1);
				text-align: right;
			}
			.new-conversation-status .call-item.right .video-btn {
				color: rgba(255, 255, 255, 1);
				text-align: left;
			}
			.new-conversation-status .call-item.right .video i {
				width: 44%;
			}
			.call-item.right .video {
				background-color: rgba(150, 204, 112, 1);
				margin-right: 10px;
				padding-right: 10px;
			}
			.call-item.right .time {
				position: inherit;
			}
			.call-item .video-text {
				margin-left: -9px;
				text-indent: 5px;
				background: url(../imgv2/message_box_gray.png) no-repeat left center;
			}
			.call-item.right .video-text {
				margin-right: -18px;
				padding-right: 20px;
				background: url(../imgv2/message_box_green.png) no-repeat right center;
			}
			.iconfont {
				margin-right: 5px;
			}
			.friend-items,
			.item-list-more {
				height: 52px;
				padding-left: 10px;
				vertical-align: middle;
			}
			.friend-items img {
				width: 36px;
				line-height: 36px;
				border-radius: 3px;
				margin-top: 6px;
			}
			.friend-items span,
			.item-list-more span {
				display: inline-block;
				height: 48px;
			}
			.friend-items i {
				display: inline-block;
				width: 100%;
				line-height: 18px;
			}
			.friend-items .friend-items-info,
			.item-list-more .item-list-more-info {
				border-bottom: 1px solid rgba(208, 208, 208, 1);
				width: 80%;
				padding-left: 5px;
				height: 40px;
			}
			.friend-items-info .friend-items-status {
				font-size: 12px;
				color: rgba(208, 208, 208, 1);
			}
			.friend-items:hover {
				background-color: rgba(255, 211, 179, 1);
			}
			.item-list-more span {
				line-height: 48px;
			}
			.item-list-more .item-list-more-icon i {
				color: rgba(255, 255, 255, 1);
				background-color: rgba(41, 163, 204, 1);
				width: 32px;
				height: 32px;
				line-height: 32px;
				border-radius: 5px;
				font-size: 22px;
				text-align: center;
				display: inline-block;
			}
			.voice_btn {
				width: 100px;
				background-color: #1e1e1e;
				border-top-style: none;
				border-right-style: none;
				border-bottom-style: none;
				border-left-style: none;
				color: #FFF;
				height: 60px;
				line-height: 60px;
				font-size: 13px;
			}
			.popup {
				background-color: #1e1e1e;
				margin-right: auto;
				margin-left: auto;
				border: 1px solid #8d8d8d;
				width: 90%;
				overflow: auto;
				position: absolute;
				z-index: 99;
				left: 4%;
				top: 10%;
			}
			.disable {
				filter: gray;
				-moz-opacity: 0.3;
				opacity: 0.3;
			}
			.user_popup {
				height: 4rem;
			}
			.user_popup .user_img {
				width: 64px;
				padding: 6px;
				text-align: left;
				margin-left: 0px;
				padding-left: 0px;
				padding-right: 0px;
				border-radius: 8px;
				margin-bottom: 0px;
				box-shadow: 1px 1px 1px rgba(222, 222, 222, 1), -1px -1px 1px rgba(222, 222, 222, 1);
				border: 1px solid rgba(222, 222, 222, 1);
			}
			.user_popup .user_img:after {
				content: '';
				width: 12px;
				height: 12px;
				margin-left: 25px;
				margin-right: auto;
				margin-top: 0px;
				position: absolute;
				-moz-transform: rotate(45deg);
				-webkit-transform: rotate(45deg);
				-o-transform: rotate(45deg);
				transform: rotate(45deg);
			}
			.user_popup .downarrow {
				text-align: center;
			}
			.user_popup .downarrow img {
				margin: auto;
			}
			.user_img p {
				text-align: center;
				white-space: nowrap;
				overflow: hidden;
				text-overflow: ellipsis;
				font-size: 12px;
				margin: 0px;
			}
			.user_img .user_popup_name {
				margin-bottom: 4px;
				margin-top: 0px;
			}
			.user_img_blue,
			.user_img_blue:after {
				background-color: rgba(41, 163, 204, 1);
				color: rgba(255, 255, 255, 1);
			}
			.user_img_normal,
			.user_img_normal:after {
				background-color: rgba(255, 255, 255, 1);
			}
			.current-location {
				position: absolute;
				left: 10px;
				bottom: 100px;
				cursor: pointer;
			}
			.current-location i {
				font-size: 3rem;
				color: rgba(23, 155, 200, 1);
			}
			.amap-zoom-touch-minus {
				bottom: -48px;
			}
			.amap-zoom-touch-plus {
				bottom: 0px;
			}
			.off-line {
				opacity: 0.4;
			}
			.friend-items-status:after {
				content: '在线';
			}
			.off-line .friend-items-status:after {
				content: '离线';
			}
			i,em{
				font-style: normal;
			}
		</style>
		<script src="../jsv2/DOMContentLoaded.js"></script>
		<% if(session.getAttribute( "openid_session")==null || session.getAttribute( "openid_session")=="" ){ 
			String path=request.getContextPath(); 
			String qString=request.getQueryString(); 
			String url=request.getScheme()+ "://"+ request.getServerName()+request.getRequestURI()+(qString==null?"":( "?"+qString)); response.sendRedirect( "../oauthSessionServlet?redirectUrl="+url); }
		%>
	</head>

	<body class="whitebg">
		<!--wrap start-->
		<section id="wrap">
			<section class="detailtitlebar">
				<img src="" width="100%" height="100%" class="tripuser" />
				<ul class="listitem h120">
					<li class="userinfo">
						<p class="username" id="owner-name"></p>
					</li>
					<li class="d_triptitle"><!--正在计算距离...--></li>
				</ul>
			</section>
			<div id="iCenter"></div>
			<div id="currentLocation" class="current-location"><i class="iconfont">&#xe601;</i>
			</div>
			<div class="btns-bottom-list">
				<span class="btns-item-friend" target="friend-list-status"><i class="icon iconfont">&#xe604;</i><i class="btn-name">好友</i></span>
				<span id="startRecord" data-status="startrecord" class="btns-item-soundrecording"><em><i class="icon iconfont">&#xe609;</i><i class="btn-name">录音</i></em></span>
				<span class="btns-item-conversation new-conversation" target="new-conversation-status"><i class="icon iconfont">&#xe608;</i><i class="btn-name">会话</i></span>
				<!--<span class="btns-item-more" target="btns-item-list-more"><i class="icon iconfont">&#xe600;</i><i class="btn-name">更多</i></span>-->
			</div>
			<div class="up-list friend-list-status">
				<div class="up-list-title">
					<span class="up-list-title-txt">好友</span> <span class="up-list-title-close" target="friend-list-status"><i class="icon iconfont">&#xe602;</i></span>
				</div>
				<div class="up-list-content friend-status">
				</div>
			</div>
			<div class="up-list new-conversation-status">
				<div class="up-list-title">
					<span class="up-list-title-txt">会话</span> <span class="up-list-title-close" target="new-conversation-status"><i class="icon iconfont">&#xe602;</i></span>
				</div>
				<div class="up-list-content new-conversation">
				</div>
			</div>
			<div class="up-list btns-item-list-more">
				<div class="up-list-title">
					<span class="up-list-title-txt">更多</span> <span class="up-list-title-close" target="btns-item-list-more"><i class="icon iconfont">&#xe602;</i></span>
				</div>
				<div class="up-list-content item-list-more">
					<div class="item-list-more" id="btnUpdatePosition">
						<span class="item-list-more-icon"><i class="icon iconfont">&#xe607;</i></span>
						<span class="item-list-more-info">更新位置</span>
					</div>
					<div class="item-list-more" id="btnStopPickup">
						<span class="item-list-more-icon"><i class="icon iconfont">&#xe603;</i></span>
						<span class="item-list-more-info">我已上车</span>
					</div>
				</div>
			</div>
		</section>
		<section class="black_bg alert-mask">
			<div class="w_popup_btn">
				<div class="message_text">正在加载中...</div>
				<div class="messagebtn">
					<button class="messagebtnone" style="background-color: transparent; border: 0;">确定</button>
				</div>
			</div>
		</section>
		<!--wrap end-->
	</body>
	<script src="../jsv2/jquery-1.10.1.min.js"></script>
	<!--<script src="../jsv2/common.js"></script>-->
	<script src="http://webapi.amap.com/maps?v=1.3&key=57c58c7b1f94514317df97e3a985962c"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	<script src="../jsv2/QueryStringParser.js"></script>
	<script src="../jsv2/AMapCommon.js"></script>
	<script src="../jsv2/jsonMsg.js"></script>
	<script src="../jsv2/lib/k-utils-fn.js"></script>
	<script src="../jsv2/journey/operateData.js"></script>
	<script src="../jsv2/car.js"></script>
<script>
		var openId, pickupId, ownerId, receiverId, pageURL, kmap, memberHashList = {},
			tripStatus = 1,
			currentUserIcon, timeIntervalClock /*计时时钟*/ , timeCountDown = 55, //录音时倒计时
			newsVoiceID /*最新一条语音ID*/ ;
		var mapObj, geolocation, markerFF, markers = [],
			lastUserDeepStatus = 'mrn',stopRecordCallback = null,
			geoSuccessListener, geoErrorListener, isDragstart = false,
			owner;
		var toolBar;
		var lastMemberLocList = [];
		var locationInfo;
		var windowsArr = [];
		var marker = [];
		var memberList = [];
		var latLon = "";
		var lat_me = "";
		var lon_me = "";
		var isLeaderFlag = false;
		 //起、终点
		var start_xy = new AMap.LngLat(116.379018, 39.865026);
		var end_xy = new AMap.LngLat(116.321139, 39.896028);
		var customMarker = new AMap.Marker({
			offset: new AMap.Pixel(-14, -34), //相对于基点的位置
			icon: new AMap.Icon({ //复杂图标
				size: new AMap.Size(27, 36), //图标大小
				image: "http://webapi.amap.com/images/custom_a_j.png", //大图地址
				imageOffset: new AMap.Pixel(-28, 0) //相对于大图的取图位置
			})
		});
		var nickName, imageURL;
		$(function() {
			showMaskLoading();
			openId = '';
			pickupId = queryString.getParam("pickupid");
			
			<%if (session.getAttribute("openid_session") == null) {%>
			openId='';
			<%} else {%>
			openId = '<%=session.getAttribute("openid_session")%>';
			<%}%>
			
			$('.btns-info').on('click', function() {
				if ($('.has-hide').is(':visible')) {
					$('.has-hide').slideUp();
					$('.location-info').addClass('hide');
					$('.btns-info').addClass('hide');
					$('.btns-info i').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
				} else {
					$('.has-hide').slideDown();
					$('.location-info').removeClass('hide');
					$('.btns-info').removeClass('hide');
					$('.btns-info i').removeClass('glyphicon-triangle-bottom').addClass('glyphicon-triangle-top');
				}
			});
			$('#iCenter').css({
				'height': (K.getPageSize()[1] - 60) + 'px'
			});
			pageURL = location.href.split('#')[0];
			if (openId == null || openId == "") {
				closemaskLoading(); //关闭这招层
				return false;
			} else {
				//setMenu(openId, tripId);
				fillTripInfo(openId, pickupId);
				loadWechatVoice();
			}
			$('.btns-bottom-list span[target]').on('click', function(e) {
				var tar = $(e.target),
					tg = tar.attr('target');
				tg = tg ? tg : tar.parent().attr('target');
				tar = $('.' + tg);
				var len = tar.attr('length');
				if (tg == 'new-conversation-status') {
					tar.css({
						'max-height': '45%',
						'height': (len ? (48 * len + 28) : document.body.clientHeight * .40) + 'px'
					});
					$('.up-list-content', tar).css({
						'max-height': document.body.clientHeight * .45 + 'px',
						'height': (len ? (48 * len) : document.body.clientHeight * .34) + 'px'
					});
					fetchVoiceList();
				} else if (tg == 'btns-item-list-more') {
					len = 2;
					tar.css({
						'max-height': '45%',
						'height': 52 * len + 28 + 'px'
					});
					$('.up-list-content', tar).css({
						'max-height': document.body.clientHeight * .42 + 'px',
						'height': 52 * len + 'px'
					});
				} else {
					len = len || 2;
					tar.css({
						'max-height': '45%',
						'height': 52 * len + 28 + 'px'
					});
					$('.up-list-content', tar).css({
						'max-height': document.body.clientHeight * .42 + 'px',
						'height': 52 * len + 'px'
					});
				}
			});
			$('.up-list-title-close').on('click', function(e) {
				var tar = $(e.target),
					tg = tar.attr('target');
				tg = tg ? tg : tar.parent().attr('target');
				if (tg == 'new-conversation-status') {
					$('.btns-item-conversation').removeClass('has-new');
				}
				$('.up-list.' + tg).css({
					'height': '0%'
				});
			});
			//选中某个用户就将该用户定位到中心点
			$(document).on('click', '.up-list-content.friend-status .friend-items', function(e) {
				var tar = $(e.target);
				tar = _getTarparanetByClass(tar, 'friend-items');
				var userInfo = tar.attr('openid');
				userInfo ? setMapCenterByOpneID(userInfo) : '';
			});
			$('#currentLocation').attr({
				'OpenID': openId
			}).on('click', function(e) {
				var tar = $(e.target);
				tar = _getTarparanetByClass(tar, 'current-location');
				var oid = tar.attr('OpenID');
				oid ? setMapCenterByOpneID(oid) : '';
			});
			$('#btnUpdatePosition').on('click', function(e) {
				updatePickupStatus('1');
			});
			$('#btnStopPickup').on('click', function(e) {
				updatePickupStatus('2');
			});
			$('.messagebtnone').on('click', function() {
				stopRecordCallback && stopRecordCallback(); //回调函数存在就调用回调函数
				stopRecordCallback = null;
				closeAlertMask();
			});
		});
		/**
		 * 将地图定位到指定用户中心
		 * @param {Object} oid 用户ID
		 */
		function setMapCenterByOpneID(oid) {
			if (memberList && oid) {
				for (var i = 0, l = memberList.length; i < l; i++) {
					if (memberList[i].OpenID == oid) { //将指定用户定位但中间
						mapObj.setZoomAndCenter(15, new AMap.LngLat(memberList[i].Loc.Longitude / 1000000, memberList[i].Loc.Latitude / 1000000));
						break;
					}
				}
			}
		}

		function setLocation() {
				if (geolocation) {
					geolocation.getCurrentPosition();
					//geolocation.watchPosition();
				} else {
					setTimeout(setLocation, 1000);
				}
			}
			//获取团员地址时，如果某个人团员下线了，则把头像从地图上移除掉

		function deleteNotInMarkers(memberData) {
				var flag = true;
				for (var i = 0, j = markers.length; i < j; i++) {
					var m = markers[i];
					flag = true;
					if (memberData && memberData.length > 0) {
						for (var _i = 0, _j = memberData.length; _i < _j; _i++) {
							var md = memberData[_i];
							if (m.getTitle() === md.OpenID) {
								flag = false;
								break;
							}
						}
					}
					if (flag) {
						m.setMap(null);
					}
				}
				memberData ? '' : markers = [];
			}
			//显示所有人的位置头像

		function showAllPositions(allPositions) {
				for (var i = 0, j = allPositions.length; i < j; i++) {
					var mP = allPositions[i];
					//if (mP.Loc.Longitude != "0" && mP.Loc.Latitude != "0" && mP.OpenID != openId) {
					if (mP && mP.Loc && mP.Loc.Longitude != "0" && mP.Loc.Latitude != "0") {
						var headImg = "";
						var nickNm = "";
						var deepstatus = 'mrn';
						var isLeader = false;
						for (var k = 0; k < memberList.length; k++) {
							if (memberList[k].OpenID == mP.OpenID) {
								headImg = memberList[k].ImageURL;
								nickNm = memberList[k].Nickname;
								deepstatus = mP.DeepStatus; //获取类型mobile为手机,mrn为汽车
								break;
							}
						}
						if (tripLeaderId == mP.OpenID) {
							isLeader = true;
							//start_xy = new AMap.LngLat(mP.Loc.Longitude / 1000000, mP.Loc.Latitude / 1000000);
						} else {
							//end_xy = new AMap.LngLat(mP.Loc.Longitude / 1000000, mP.Loc.Latitude / 1000000);
						}
						setMemberLocation(mP.OpenID, mP.Loc.Longitude / 1000000, mP.Loc.Latitude / 1000000, headImg, isLeader, 20, nickNm, deepstatus);
					}
				}
				/*if (allPositions.length > 1) {
					//driving_route();
					$('.d_triptitle').html('无法计算距离!');
				} else {
					for (var k = 0; k < memberList.length; k++) {
						if (memberList[k].OpenID != openId) {
							$('.d_triptitle').html(memberList[k].Nickname + ' 未上线，无法计算距离!');
							break;
						}
					}
				}*/
				//$('.d_triptitle').html('无法计算距离!');
			}
			//当开着这个页面的时候，上传位置信息
		var isFirst = true;

		function updateMemberPositions(_lat, _lng) {
			var loc = {
				Latitude: 29.579916,
				Longitude: 115.98905
			};
			loc.Latitude = parseFloat(_lat) * 1000000;
			loc.Longitude = parseFloat(_lng) * 1000000;
			/*var dynamicInfo = {
				ActiveTripID: pickupId,
				Loc: loc
			};*/
			var dataStr = {
				PickupID: pickupId,
				OpenID: receiverId
			};
			//车主（就是被请求人）
			if (openId == receiverId) {
				dataStr['Loc'] = loc;
			}
			bidgeToWeixin(openId, 'DynamicInfoPickup', dataStr, function(data, status, xhr) {
				lastMemberLocList = data.portalResp.Data;
				//对应请求人，好友的状态 是通过DynamicInfoPickupResp返回中是否有Loc这个字段判断的，
				//如果有这个 说明被请求人在提交位置，说明被请求人在线。 请求人自己的位置就是搭车里面的经纬度就可以
				//先将对方设置为不在线
				$('.friend-items[openid!=' + openId + ']').addClass('off-line');
				if (data && data.portalResp && data.portalResp.Data && data && data.portalResp && data.portalResp.Data.Loc) {
					$('.friend-items[openid=' + receiverId + ']').removeClass('off-line');
				}
				//对于被请求人，请求人状态 通过DynamicInfoPickupResp返回中是否有SenderFlag这个字段判断的. 
				//如果有这个，显示请求人在线
				if (data && data.portalResp && data.portalResp.Data && data && data.portalResp && data.portalResp.Data.SenderFlag) {
					$('.friend-items[openid=' + ownerId + ']').removeClass('off-line');
				}
				//当前进入用户为请求人
				if (openId == ownerId&&data && data.portalResp && data.portalResp.Data && data && data.portalResp && data.portalResp.Data.Loc) {
					for (var k = 0; k < memberList.length; k++) {
						if (memberList[k].OpenID ==receiverId) {
							memberList[k].Loc={
									Longitude:data.portalResp.Data.Loc.Longitude,
									Latitude:data.portalResp.Data.Loc.Latitude
							}
							setMemberLocation(receiverId, data.portalResp.Data.Loc.Longitude / 1000000,data.portalResp.Data.Loc.Latitude / 1000000,
									memberList[k].ImageURL, true, 20, memberList[k].Nickname, 'mrn');
							break;
						}
					}
				}
				//清除界面所有的marker图层
						///	var pUser = [owner, data.portalResp.Data];
				//deleteNotInMarkers(data.portalResp.Data);
				//data && data.portalResp && data.portalResp.Data ? showAllPositions( data.portalResp.Data) : '';
				//deleteNotInMarkers(data.portalResp.Data);
			});
		}

		function showPicClickLoc() {
			/*$('.friend-items[openid!=' + openId + ']').addClass('off-line'); //非当前用户都默认离线
			if (lastMemberLocList && lastMemberLocList.length) {
				for (var i = 0, j = lastMemberLocList.length; i < j; i++) {
					var mP = lastMemberLocList[i];
					//if (mP.Loc.Longitude != "0" && mP.Loc.Latitude != "0" && mP.OpenID != openId) {
					if (mP.Loc.Longitude != "0" && mP.Loc.Latitude != "0") {
						$('.friend-items[openid=' + mP.OpenID + ']').removeClass('off-line');
					}
				}
			}*/
		}

		function geoInit(isCenter) {
			geolocation = new AMap.Geolocation({
				enableHighAccuracy: true, //是否使用高精度定位，默认:true
				timeout: 100000, //超过10秒后停止定位，默认：无穷大
				maximumAge: 0, //定位结果缓存0毫秒，默认：0
				convert: true, //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
				showButton: false, //显示定位按钮，默认：true
				buttonPosition: 'LB', //定位按钮停靠位置，默认：'LB'，左下角
				buttonOffset: new AMap.Pixel(10, 20), //定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
				showMarker: false, //定位成功后在定位到的位置显示点标记，默认：true
				showCircle: false, //定位成功后用圆圈表示定位精度范围，默认：true
				panToLocation: isCenter, //定位成功后将定位到的位置作为地图中心点，默认：true
				zoomToAccuracy: true //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
			});
			mapObj.addControl(geolocation);
			geoSuccessListener = AMap.event.addListener(geolocation, 'complete', OnComplete); //返回定位信息,初始时定位后设为地图中心点
			geoErrorListener = AMap.event.addListener(geolocation, 'error', onError); //返回定位出错信息
		}

		function geoRemove() {
			mapObj.removeControl(geolocation);
			AMap.event.removeListener(geoSuccessListener);
			AMap.event.removeListener(geoErrorListener);
			if ($("div[title='locationIMG']")) {
				$("div[title='locationIMG']").parent().remove();
			}
		}

		function mapInit(center) {
			mapObj = new AMap.Map('iCenter', {
				level: 15
			});
			mapObj.plugin(["AMap.ToolBar"], function() {
				toolBar = new AMap.ToolBar();
				mapObj.addControl(toolBar);
			});
			mapObj.plugin('AMap.Geolocation', function() {
				geoInit(false);
			});
			AMap.event.addListener(mapObj, 'dragstart', function(e) { //移动地图后再定位时不设为地图中心点
				isDragstart = true;
				geoRemove();
				geoInit(false);
				//setLocation()
			});
			//var pageURL = location.href.split('#')[0];
			//pageURL = pageURL.substring(0,pageURL.indexOf("trip_voice"))+
			//终点
			/*var endmarker = new AMap.Marker({
				icon: new AMap.Icon({ //复杂图标
					image: "../imgv2/dis_icon.png" //大图地址
				})
			});*/
			//endmarker.setPosition(center);
			//endmarker.setMap(mapObj);
			mapObj.setZoomAndCenter(14, center);
		};

		function getExistsInMarkers(titleOpenID) {
			for (var i = 0; i < markers.length; i++) {
				var m = markers[i];
				if (m.getTitle() === titleOpenID) {
					return m;
				}
			}
			return null;
		}

		function setMemberLocation(_oID, _lng, _lat, _imageUrl, _isLeader, currentMember, nickNameIn, deepstatus) {
			var _m = getExistsInMarkers(_oID);
			_m ? _m.setMap(null) : '';
			deepstatus = (deepstatus ? deepstatus : 'mrn') == 'mrn' ? 'pin_car' : 'pin_phone';
			if (_m) {
				if (currentMember > 1) {
					_m.setzIndex(currentMember);
				}
				_m.setPosition(new AMap.LngLat(_lng, _lat));
				_m.setMap(mapObj);
			} else {
				var marker = new AMap.Marker({
					//icon: '../imgv2/' + deepstatus + '.png',
					position: new AMap.LngLat(_lng, _lat),
					title: _oID
				});
				//自定义点标记内容
				var markerContent = document.createElement("div");
				markerContent.className = "user_popup";
				//头像
				var markerContentDiv2 = document.createElement("div");
				if (_isLeader) {
					markerContentDiv2.className = "user_img user_img_blue";
				} else {
					markerContentDiv2.className = "user_img user_img_normal";
				}
				var markerP2 = document.createElement("p");
				var markerImg2 = document.createElement("img");
				markerImg2.src = _imageUrl !== null && _imageUrl !== "" && _imageUrl != undefined ? _imageUrl : "../imgv2/friend_pic.png";
				markerImg2.width = 50;
				markerImg2.height = 50;
				markerP2.appendChild(markerImg2);
				markerContentDiv2.appendChild(markerP2);
				markerContent.appendChild(markerContentDiv2);
				//向下的箭头不需要
				var markerContentDiv3 = document.createElement("div");
				markerContentDiv3.className = "downarrow";
				var markerImg3 = document.createElement("img");
				markerImg3.src = '../imgv2/' + deepstatus + '.png';
				markerContentDiv3.appendChild(markerImg3);
				markerContent.appendChild(markerContentDiv3);
				if (currentMember > 1) {
					marker.setzIndex(currentMember);
				}
				marker.setContent(markerContent); //更新点标记内容
				marker.setMap(mapObj);
				mapObj.setZoomAndCenter(10, new AMap.LngLat(_lng, _lat));
				mapObj.setFitView();
				markers.push(marker);
			}
		}

		function OnComplete(data) {
				lat_me = data.position.getLat();
				lon_me = data.position.getLng();
				//setMemberLocation(openId, data.position.getLng(), data.position.getLat(), imageURL, tripLeaderId == openId ? true : false, 20, nickName, 'mrn');
				updateMemberPositions(data.position.getLat(), data.position.getLng());
				//setMemberLocation(mP.OpenID, mP.Loc.Longitude / 1000000, mP.Loc.Latitude / 1000000, headImg, isLeader, 20, nickNm, deepstatus);
				//receiverId
					for (var k = 0; k < memberList.length; k++) {
							if (memberList[k].OpenID ==receiverId) {
								//deepstatus = mP.DeepStatus; //获取类型mobile为手机,mrn为汽车
								memberList[k].Loc={
										Longitude:lon_me*1000000,
										Latitude:lat_me*1000000
								}
								setMemberLocation(receiverId, lon_me, lat_me,  memberList[k].ImageURL, true, 20, memberList[k].Nickname, 'mrn');
								break;
							}
						}
			}
			/*
			 *解析定位错误信息
			 */

		function onError(data) {
			var str = '定位失败,';
			str += '错误信息：'
			switch (data.info) {
				case 'PERMISSION_DENIED':
					str += '浏览器阻止了定位操作';
					break;
				case 'POSITION_UNAVAILBLE':
					str += '无法获得当前位置';
					break;
				case 'TIMEOUT':
					str += '定位超时';
					break;
				default:
					str += '未知错误';
					break;
			}
			updateMemberPositions(0, 0);
			setLocation();
		}

		function openMarkerTipById1(pointid, thiss) {
				thiss.style.background = '#CAE1FF';
				windowsArr[pointid].open(mapObj, marker[pointid]);
			}
			//添加查询结果的marker&infowindow

		function addmarker(i, d) {
				var lngX = d.location.getLng();
				var latY = d.location.getLat();
				var markerOption = {
					map: mapObj,
					icon: "http://webapi.amap.com/images/" + (i + 1) + ".png",
					position: new AMap.LngLat(lngX, latY)
				};
				var mar = new AMap.Marker(markerOption);
				marker.push(new AMap.LngLat(lngX, latY));
				var infoWindow = new AMap.InfoWindow({
					content: "<h3><font color=\"#00a6ac\">  " + (i + 1) + ". " + d.name + "</font></h3>" + TipContents(d.type, d.address, d.tel),
					size: new AMap.Size(300, 0),
					autoMove: true,
					offset: new AMap.Pixel(0, -30)
				});
				windowsArr.push(infoWindow);
				var aa = function(e) {
					infoWindow.open(mapObj, mar.getPosition());
				};
				AMap.event.addListener(mar, "click", aa);
			}
			/**
			 * 修改结束行程操作已经移除出去了
			 */

		function popMore() {}

		function uploadVoiceId(voiceId) {
			var dataStr = {
				"TripID": parseInt(pickupId, 10),
				"VoiceType": 2,
				"VoiceID": voiceId
			};
			bidgeToWeixin(openId, 'UploadVoice', dataStr, function(data, status, xhr) {
				//alert("上传语音成功!");
			});
		}
		var voice = {
			localId: '',
			serverId: ''
		};

		function startVoice(voiceId) {
			voice.serverId = voiceId;
			wx.downloadVoice({
				serverId: voice.serverId,
				success: function(res) {
					voice.localId = res.localId;
					if (voice.localId == '') {
						showAlertMask({
							title: '没有最新的语音信息！'
						});
						return;
					}
					wx.playVoice({
						localId: voice.localId
					});
				}
			});
		}

		function loadWechatVoice() {
			var dataStr = {
				PageURL: pageURL
			};
			bidgeToWeixin(openId, 'JSSignature', dataStr, function(data, status, xhr) {
				var voiceData = data && data.portalResp && data.portalResp.Data;
				if (!voiceData) {
					return false;
				}
				var timestamp = voiceData.timestamp;
				var noncestr = voiceData.noncestr;
				var signature = voiceData.signature;
				var appId = voiceData.appId;
				wx.config({
					debug: false,
					appId: appId,
					timestamp: timestamp,
					nonceStr: noncestr,
					signature: signature,
					jsApiList: [
						'hideOptionMenu',
						'showOptionMenu',
						'stopRecord',
						'onRecordEnd',
						'playVoice',
						'pauseVoice',
						'stopVoice',
						'uploadVoice',
						'downloadVoice',
						'startRecord'
					]
				});
				wx.ready(function() {
					wx.hideOptionMenu();
					enableWechatJS();
				});
				wx.error(function(res) {
					//alert(res.errMsg);
				});
			});
		}

		function enableWechatJS() {
			wx.ready(function() {
				// 4 音频接口
				// 4.2 开始录音
				$('#startRecord').on('click', function(e) {
					var tar = $(e.target);
					tar = _getTarparanetByClass(tar, 'btns-item-soundrecording');
					var btnStatus = tar.attr('data-status');
					if (btnStatus == 'startrecord') {
						$('.btn-name', tar).html("停止录音");
						tar.attr({
							'data-status': 'stoprecord'
						});
						updateCountdown(); //开始调用
						timeIntervalClock = setInterval(updateCountdown, 1000);
						wx.startRecord({
							cancel: function() {
								showAlertMask({
									title: '用户拒绝授权录音'
								});
							}
						});
					} else {
						$('.btn-name', tar).html("录音");
						tar.attr({
							'data-status': 'startrecord'
						});
						timeCountDown = 55;
						clearInterval(timeIntervalClock);
						wx.stopRecord({
							success: function(res) {
								voice.localId = res.localId;
								if (voice.localId == '') {
									showAlertMask({
										title: '请先点击开始录音按钮录制一段声音'
									});
									return;
								}
								wx.uploadVoice({
									localId: voice.localId,
									isShowProgressTips: 1, // 默认为1，显示进度提示
									success: function(res) {
										//alert('上传语音成功，serverId 为' + res.serverId);
										voice.serverId = res.serverId;
										uploadVoiceId(res.serverId);
									}
								});
							},
							fail: function(res) {
								//alert(JSON.stringify(res));
							}
						});
					}
				});
				wx.onVoiceRecordEnd({
					complete: function(res) {
						voice.localId = res.localId;
						showAlertMask({
							title: '录音时间已超过一分钟'
						});
					}
				});
				wx.onVoicePlayEnd({
					complete: function(res) {
						//alert('录音（' + res.localId + '）播放结束');
					}
				});
			});
			wx.error(function(res) {
				//alert(res.errMsg);
			});
		}
		var nickName, imageURL;

		function hiddenVoiceSession() {}

		function fetchVoiceList() {
				var dataStr = {
					"TripID": parseInt(pickupId, 10),
					"VoiceType": 2,
					"Cursor": 0,
					"Limit": 10,
					"OpenID": openId
				};
				bidgeToWeixin(openId, 'ListVoice', dataStr, function(data, status, xhr) {
					var voiceMsg = [],
						voices = data && data.portalResp && data.portalResp.Data && data.portalResp.Data.Voices;
					newsVoiceID = voices.length > 0 ? voices[0].VoiceID : '';
					$.each(voices, function(index) {
						var lastestOpenId = (voices[index].OpenID);
						var lastestImage = (voices[index].ImageURL);
						var lastestVoiceID = (voices[index].VoiceID);
						var lastestNickName = (voices[index].NickName);
						var createTime = (voices[index].createTime || '');
						if (lastestOpenId != openId) {
							voiceMsg.push('<div class="call-item" onclick="startVoice(\'' + lastestVoiceID + '\')">');
							voiceMsg.push('<span class="header-img"><img src="' + lastestImage + '" /></span>');
							voiceMsg.push('<span class="video">');
							voiceMsg.push('<i class="video-text icon iconfont">&#xe60b;</i>');
							voiceMsg.push('<i class="video-btn icon iconfont">&#xe605;</i>');
							voiceMsg.push('</span>');
							voiceMsg.push('<span class="time">' + (createTime.replace('AM', '上午').replace('PM', '下午')) + '</span>');
							voiceMsg.push('</div>');
						} else {
							voiceMsg.push('<div class="call-item right" onclick="startVoice(\'' + lastestVoiceID + '\')">');
							voiceMsg.push('<span class="header-img"><img src="' + lastestImage + '" /></span>');
							voiceMsg.push('<span class="video" onclick="startVoice(\'' + lastestVoiceID + '\')">');
							voiceMsg.push('<i class="video-btn icon iconfont">&#xe605;</i>');
							voiceMsg.push('<i class="video-text icon iconfont">&#xe60a;</i>');
							voiceMsg.push('</span>');
							voiceMsg.push('<span class="time">' + (createTime.replace('AM', '上午').replace('PM', '下午')) + '</span>')
							voiceMsg.push('</div>');
						}
					});
					$('.up-list-content.new-conversation').html(voiceMsg.join(''));
				});
			}
			//@todo是否需要该函数？2015-04-14

		function fillTripInfo(openId, pickupId) {
				var dataStr = {
					PickupID: pickupId
				};
				bidgeToWeixin(openId, 'PickupDetail', dataStr, function(rdata, status, xhr) {
					var data = rdata && rdata.portalResp && rdata.portalResp.Data;
					$('#owner-name').html((data.SenderName || '') + ' 距离 ' + data.ReceiverName);
					tripLeaderId = data.Receiver;
					receiverId = data.Receiver;
					nickName = data.SenderName;
					imageURL = data.SenderPortrait;
					ownerId = data.OwnerId;
					$('.tripuser').attr({
						'src': imageURL
					});
					var memberStr = [];
					memberList.push({
						OpenID: data.OwnerId,
						ImageURL: data.SenderPortrait,
						Nickname: data.SenderName,
						Loc:data.Loc,
						DeepStatus:'mobile'
					});
					memberList.push({
						OpenID: data.Receiver,
						ImageURL: data.ReceiverPortrait,
						Nickname: data.ReceiverName,
						Loc:null,
						DeepStatus:'mrn'
					});
					owner = memberList[0];
					owner['Loc'] = data.Loc;
					//创建用户
					memberStr.push($('<div>').addClass('friend-items ' + (data.OwnerId == openId ? '' : 'off-line'))
						.attr({
							'openid': data.OwnerId
						})
						.append($('<img>').attr({
							'src': data.SenderPortrait
						}))
						.append($('<span>').addClass('friend-items-info')
							.append($('<i>').addClass('friend-items-name').html(data.SenderName))
							.append($('<i>').addClass('friend-items-status').html(''))
						));
					//接受用户
					memberStr.push($('<div>').addClass('friend-items ' + (data.Receiver == openId ? '' : 'off-line'))
						.attr({
							'openid': data.Receiver
						})
						.append($('<img>').attr({
							'src': data.ReceiverPortrait
						}))
						.append($('<span>').addClass('friend-items-info')
							.append($('<i>').addClass('friend-items-name').html(data.ReceiverName))
							.append($('<i>').addClass('friend-items-status').html(''))
						));
					//调整好友列表显示的顺序问题
					if (data.Receiver == openId) {
						memberStr.reverse(); //数组反转
						//当前用户是请求人，就不更新用户位置信息
						setLocation();
						setInterval(setLocation, 20000);
					} else {
						//当前用户是发送用户的话，就直接调用更新位置获取对方的位置信息
						setInterval(updateMemberPositions, 60000);
					}
					$('.up-list.friend-list-status').attr({
						'length': 2
					});
					$('.up-list-content.friend-status').empty().append(memberStr);
					mapInit(new AMap.LngLat(data.Loc.Longitude / 1000000, data.Loc.Latitude / 1000000));
					lat_me = data.Loc.Latitude / 1000000;
					lon_me = data.Loc.Longitude / 1000000;
					mapObj.setZoomAndCenter(14, new AMap.LngLat(data.Loc.Longitude / 1000000, data.Loc.Latitude / 1000000));
					showAllPositions([{
						OpenID: data.OwnerId,
						ImageURL: data.SenderPortrait,
						Nickname: data.SenderName,
						DeepStatus:'mobile',
						Loc: data.Loc
					}]); //显示创建者搭车位置
					closemaskLoading(); //关闭这招层
				});
			}
			/**
			 * 每次获取最新一条语音，ID号与上一次一致就不提醒，不一致就提醒
			 */

		function fetchVoiceListByLimit1() {
			var dataStr = {
				"TripID": parseInt(pickupId, 10),
				"VoiceType": 2,
				"Cursor": 0,
				"Limit": 1,
				"OpenID": openId
			};
			bidgeToWeixin(openId, 'ListVoice', dataStr, function(data, status, xhr) {
				var voices = data && data.portalResp && data.portalResp.Data && data.portalResp.Data.Voices;
				if (voices.length > 0 && ((!newsVoiceID) || (newsVoiceID && newsVoiceID != voices[voices.length - 1].VoiceID))) {
					$('.btns-item-conversation').addClass('has-new');
					//newsVoiceID = voices[voices.length - 1].VoiceID;
				} else {
					$('.btns-item-conversation').removeClass('has-new');
				}
			});
		}

		function updatePickupStatus(status) {
			var loc = {
				Latitude: 29579916,
				Longitude: 11598905
			};
			loc.Latitude = parseFloat(String(lat_me)) * 1000000;
			loc.Longitude = parseFloat(String(lon_me)) * 1000000;
			var dataStr = {
				"PickupID": pickupId,
				"Status": status,
				"Loc": loc
			};
			bidgeToWeixin(openId, 'UpdateStatusPickup', dataStr, function(data, status, xhr) {
				//console.log(data);
			});
		}
		var soundRecordingCountDown = $('.btns-item-soundrecording .btn-name');
		/**
		 * 录音倒计时,注意结束时调用事件的顺序
		 * 此处为，调用alert前已经停止录音，点击确定后上传录音数据,同时改变按钮状态
		 */
		function updateCountdown() {
				var html = soundRecordingCountDown.html(),
					startIndex = html.indexOf('('),
					endIndex = html.indexOf(')'),
					strHTML = '';
				startIndex > -1 ? strHTML = html.substr(0, startIndex) : strHTML = html;
				soundRecordingCountDown.html(strHTML + '(' + (timeCountDown--) + ')');
				if (timeCountDown == -1) {
					//先调用停止录音事件,获取voiceid值
					wx.stopRecord({
						success: function(res) {
							voice.localId = (res && res.localId) || '';
							if (voice.localId == '') {
								showAlertMask({
									title: '请先点击开始录音按钮录制一段声音'
								});
								return;
							}
						},
						fail: function(res) {
							//alert(JSON.stringify(res));
						}
					});
					//弹出提示信息窗口
					timeCountDown = 55;
					clearInterval(timeIntervalClock);
					showAlertMask({title:'已达到最长录音时间，录音自动停止。点击确定将自动提交语音。'});
					//alert('已达到最长录音时间，录音自动停止。点击确定将自动提交语音。');
					stopRecordCallback = function() {
						//点击确定后修改按钮状态，同时上传录音文件
						$('.btn-name', $('#startRecord')).html("录音");
						$('#startRecord').attr({
							'data-status': 'startrecord'
						});
						wx.uploadVoice({
							localId: voice.localId,
							isShowProgressTips: 1, // 默认为1，显示进度提示
							success: function(res) {
								voice.serverId = res.serverId;
								uploadVoiceId(res.serverId);
							}
						});
					}
				}
			}
			//驾车导航

		function driving_route() {
				var MDrive;
				AMap.service(["AMap.Driving"], function() {
					var DrivingOption = {
						//驾车策略，包括 LEAST_TIME，LEAST_FEE, LEAST_DISTANCE,REAL_TRAFFIC
						policy: AMap.DrivingPolicy.LEAST_TIME
					};
					MDrive = new AMap.Driving(DrivingOption); //构造驾车导航类 
					//根据起终点坐标规划驾车路线
					MDrive.search(start_xy, end_xy, function(status, result) {
						if (status === 'complete' && result.info === 'OK') {
							//driving_routeCallBack(result);
							$('.d_triptitle').html(parseFloat(result.routes[0]['distance'] / 1000).toFixed(2) + '公里，预计' + parseFloat(result.routes[0]['time'] / 60).toFixed(0) + '分钟到达');
						} else {
							//alert(result);
						}
					});
				});
			}
			//setLocation();
			//setInterval(setLocation, 20000);
			//setInterval(showPicClickLoc, 2000); //更新用户状态及地理位置//不需要更新，第一加载后就不会变化
		setInterval(fetchVoiceListByLimit1, 60000); //每一分钟获取一次新的语音
	</script>

</html>